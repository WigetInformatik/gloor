object FormAuftragSuchen: TFormAuftragSuchen
  Left = 0
  Top = 0
  Caption = 'Auftrag suchen'
  ClientHeight = 861
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    1264
    861)
  PixelsPerInch = 96
  TextHeight = 13
  object LabelKunde: TLabel
    Left = 40
    Top = 32
    Width = 34
    Height = 13
    Caption = 'Kunde:'
  end
  object LabelAbNr: TLabel
    Left = 40
    Top = 59
    Width = 32
    Height = 13
    Caption = 'AB-Nr:'
  end
  object LabelArtikelNr: TLabel
    Left = 40
    Top = 86
    Width = 49
    Height = 13
    Caption = 'Artikel-Nr:'
  end
  object LabelDurchmesser: TLabel
    Left = 40
    Top = 113
    Width = 12
    Height = 13
    Caption = #216':'
  end
  object LabelZaehne: TLabel
    Left = 40
    Top = 140
    Width = 34
    Height = 13
    Caption = 'Z'#228'hne:'
  end
  object LabelStueck: TLabel
    Left = 40
    Top = 167
    Width = 30
    Height = 13
    Caption = 'St'#252'ck:'
  end
  object LabelAuftragsart: TLabel
    Left = 40
    Top = 194
    Width = 60
    Height = 13
    Caption = 'Auftragsart:'
  end
  object ComboBoxKunde: TComboBox
    Left = 120
    Top = 29
    Width = 145
    Height = 21
    TabOrder = 0
    Text = 'ComboBoxKunde'
    OnKeyPress = ComboBoxKundeKeyPress
  end
  object EditAbNr: TEdit
    Left = 120
    Top = 56
    Width = 145
    Height = 21
    TabOrder = 1
    OnKeyPress = ComboBoxKundeKeyPress
  end
  object EditArtikelNr: TEdit
    Left = 120
    Top = 83
    Width = 145
    Height = 21
    TabOrder = 2
    OnKeyPress = ComboBoxKundeKeyPress
  end
  object EditDurchmesser: TEdit
    Left = 120
    Top = 110
    Width = 145
    Height = 21
    TabOrder = 3
    OnKeyPress = ComboBoxKundeKeyPress
  end
  object EditZaehne: TEdit
    Left = 120
    Top = 137
    Width = 145
    Height = 21
    TabOrder = 4
    OnKeyPress = ComboBoxKundeKeyPress
  end
  object EditStueck: TEdit
    Left = 120
    Top = 164
    Width = 145
    Height = 21
    TabOrder = 5
    OnKeyPress = ComboBoxKundeKeyPress
  end
  object ComboBoxAuftragsart: TComboBox
    Left = 120
    Top = 191
    Width = 145
    Height = 21
    Style = csDropDownList
    TabOrder = 6
    OnKeyPress = ComboBoxKundeKeyPress
  end
  object AdvStringGridAuftrag: TAdvStringGrid
    Left = 16
    Top = 240
    Width = 1225
    Height = 557
    Cursor = crDefault
    Anchors = [akLeft, akTop, akBottom]
    DrawingStyle = gdsClassic
    RowCount = 1
    FixedRows = 0
    ScrollBars = ssBoth
    TabOrder = 7
    HoverRowCells = [hcNormal, hcSelected]
    OnClickCell = AdvStringGridAuftragClickCell
    OnDblClickCell = AdvStringGridAuftragDblClickCell
    ActiveCellFont.Charset = DEFAULT_CHARSET
    ActiveCellFont.Color = clWindowText
    ActiveCellFont.Height = -11
    ActiveCellFont.Name = 'Tahoma'
    ActiveCellFont.Style = [fsBold]
    ControlLook.FixedGradientHoverFrom = clGray
    ControlLook.FixedGradientHoverTo = clWhite
    ControlLook.FixedGradientDownFrom = clGray
    ControlLook.FixedGradientDownTo = clSilver
    ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
    ControlLook.DropDownHeader.Font.Color = clWindowText
    ControlLook.DropDownHeader.Font.Height = -11
    ControlLook.DropDownHeader.Font.Name = 'Tahoma'
    ControlLook.DropDownHeader.Font.Style = []
    ControlLook.DropDownHeader.Visible = True
    ControlLook.DropDownHeader.Buttons = <>
    ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
    ControlLook.DropDownFooter.Font.Color = clWindowText
    ControlLook.DropDownFooter.Font.Height = -11
    ControlLook.DropDownFooter.Font.Name = 'Tahoma'
    ControlLook.DropDownFooter.Font.Style = []
    ControlLook.DropDownFooter.Visible = True
    ControlLook.DropDownFooter.Buttons = <>
    Filter = <>
    FilterDropDown.Font.Charset = DEFAULT_CHARSET
    FilterDropDown.Font.Color = clWindowText
    FilterDropDown.Font.Height = -11
    FilterDropDown.Font.Name = 'Tahoma'
    FilterDropDown.Font.Style = []
    FilterDropDown.TextChecked = 'Checked'
    FilterDropDown.TextUnChecked = 'Unchecked'
    FilterDropDownClear = '(All)'
    FilterEdit.TypeNames.Strings = (
      'Starts with'
      'Ends with'
      'Contains'
      'Not contains'
      'Equal'
      'Not equal'
      'Larger than'
      'Smaller than'
      'Clear')
    FixedRowHeight = 22
    FixedFont.Charset = DEFAULT_CHARSET
    FixedFont.Color = clWindowText
    FixedFont.Height = -11
    FixedFont.Name = 'Tahoma'
    FixedFont.Style = [fsBold]
    FloatFormat = '%.2f'
    HoverButtons.Buttons = <>
    HoverButtons.Position = hbLeftFromColumnLeft
    HTMLSettings.ImageFolder = 'images'
    HTMLSettings.ImageBaseName = 'img'
    PrintSettings.DateFormat = 'dd/mm/yyyy'
    PrintSettings.Font.Charset = DEFAULT_CHARSET
    PrintSettings.Font.Color = clWindowText
    PrintSettings.Font.Height = -11
    PrintSettings.Font.Name = 'Tahoma'
    PrintSettings.Font.Style = []
    PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
    PrintSettings.FixedFont.Color = clWindowText
    PrintSettings.FixedFont.Height = -11
    PrintSettings.FixedFont.Name = 'Tahoma'
    PrintSettings.FixedFont.Style = []
    PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
    PrintSettings.HeaderFont.Color = clWindowText
    PrintSettings.HeaderFont.Height = -11
    PrintSettings.HeaderFont.Name = 'Tahoma'
    PrintSettings.HeaderFont.Style = []
    PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
    PrintSettings.FooterFont.Color = clWindowText
    PrintSettings.FooterFont.Height = -11
    PrintSettings.FooterFont.Name = 'Tahoma'
    PrintSettings.FooterFont.Style = []
    PrintSettings.PageNumSep = '/'
    SearchFooter.FindNextCaption = 'Find &next'
    SearchFooter.FindPrevCaption = 'Find &previous'
    SearchFooter.Font.Charset = DEFAULT_CHARSET
    SearchFooter.Font.Color = clWindowText
    SearchFooter.Font.Height = -11
    SearchFooter.Font.Name = 'Tahoma'
    SearchFooter.Font.Style = []
    SearchFooter.HighLightCaption = 'Highlight'
    SearchFooter.HintClose = 'Close'
    SearchFooter.HintFindNext = 'Find next occurrence'
    SearchFooter.HintFindPrev = 'Find previous occurrence'
    SearchFooter.HintHighlight = 'Highlight occurrences'
    SearchFooter.MatchCaseCaption = 'Match case'
    SearchFooter.ResultFormat = '(%d of %d)'
    SortSettings.DefaultFormat = ssAutomatic
    Version = '8.3.5.2'
    ColWidths = (
      64
      64
      64
      64
      64)
  end
  object ButtonCancel: TButton
    Left = 688
    Top = 820
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'Abbrechen'
    TabOrder = 8
    OnClick = ButtonCancelClick
  end
  object ButtonOk: TButton
    Left = 568
    Top = 820
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'OK'
    TabOrder = 9
    OnClick = ButtonOkClick
  end
  object ButtonSuchen: TButton
    Left = 296
    Top = 27
    Width = 177
    Height = 50
    Caption = 'Suchen'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    OnClick = UpdateAuftrag
  end
  object FDPhysMSAccessDriverLink: TFDPhysMSAccessDriverLink
    Left = 320
    Top = 24
  end
  object FDConnection: TFDConnection
    Params.Strings = (
      'DriverID=MSAcc')
    LoginPrompt = False
    Left = 320
    Top = 88
  end
  object FDMSAccessService: TFDMSAccessService
    Database = 'Gloor.accdb'
    Left = 320
    Top = 160
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    ScreenCursor = gcrHourGlass
    Left = 448
    Top = 24
  end
  object FDGUIxErrorDialog: TFDGUIxErrorDialog
    Provider = 'Forms'
    Caption = 'FireDAC Executor Error'
    Left = 448
    Top = 88
  end
  object FDGUIxLoginDialog: TFDGUIxLoginDialog
    Provider = 'Forms'
    Left = 448
    Top = 144
  end
  object FDQueryKunde: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT * FROM Kunden')
    Left = 568
    Top = 16
  end
  object FDQueryAuftragsart: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT * FROM Auftragsart')
    Left = 568
    Top = 88
  end
  object FDQueryGetAuftrag: TFDQuery
    Connection = FDConnection
    Left = 680
    Top = 16
  end
end
