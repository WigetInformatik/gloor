object DataModuleGloor: TDataModuleGloor
  OldCreateOrder = False
  Height = 556
  Width = 687
  object FDPhysMSAccessDriverLink: TFDPhysMSAccessDriverLink
    Left = 72
    Top = 32
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    ScreenCursor = gcrHourGlass
    Left = 552
    Top = 40
  end
  object FDGUIxErrorDialog: TFDGUIxErrorDialog
    Provider = 'Forms'
    Caption = 'FireDAC Executor Error'
    Left = 552
    Top = 104
  end
  object FDGUIxLoginDialog: TFDGUIxLoginDialog
    Provider = 'Forms'
    Left = 552
    Top = 160
  end
  object FDMSAccessService: TFDMSAccessService
    Database = 'Gloor.accdb'
    Left = 72
    Top = 168
  end
  object FDConnection: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Andre\Documents\Embarcadero\Studio\Projekte\Gl' +
        'oor AG\Gloor.accdb'
      'DriverID=MSAcc')
    Connected = True
    Left = 72
    Top = 96
  end
  object FDQueryAbteilungen: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT * FROM Abteilungen')
    Left = 216
    Top = 24
  end
end
