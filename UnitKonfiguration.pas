unit UnitKonfiguration;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
  VCLTee.TeCanvas;

type
  TFormKonfiguration = class(TForm)
    ButtonOk: TButton;
    ButtonCancel: TButton;
    LabelDbPath: TLabel;
    LabelKonfiguration: TLabel;
    OpenDialog: TOpenDialog;
    EditDbName: TEdit;
    btnOpen: TSpeedButton;
    ColorDialog: TColorDialog;
    ButtonColorNeu: TButtonColor;
    ButtonColorExpress: TButtonColor;
    ButtonColorReklamationen: TButtonColor;
    LabelVorschau: TLabel;
    EditVorschau: TEdit;
    ButtonColorUeberbuchung: TButtonColor;
    LabelLoeschenErlaubt: TLabel;
    CheckBoxLoeschenErlaubt: TCheckBox;
    procedure ButtonCancelClick(Sender: TObject);
    procedure ButtonOkClick(Sender: TObject);
    procedure ComboBoxDbClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ButtonColorNeuClick(Sender: TObject);
    procedure ButtonColorExpressClick(Sender: TObject);
    procedure ButtonColorReklamationenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonColorUeberbuchungClick(Sender: TObject);
  private
    { Private-Deklarationen }
    FDbName : String;
    FColorNeu : Integer;
    FColorExpress : Integer;
    FColorReklamation : Integer;
    FColorUeberbuchung : Integer;
    FVorschau : Integer;
    FLoeschenErlaubt : Boolean;
    FAbteilung : String;
  public
    { Public-Deklarationen }
    property DbName : String read FDbName;
    property ColorNeu : Integer read FColorNeu;
    property ColorExpress : Integer read FColorExpress;
    property ColorReklamation : Integer read FColorReklamation;
    property ColorUeberbuchung : Integer read FColorUeberbuchung;
    property Vorschau : Integer read FVorschau;
    property LoeschenErlaubt : Boolean read FLoeschenErlaubt;
    property Abteilung : String read FAbteilung;
  end;

var
  FormKonfiguration: TFormKonfiguration;

implementation

uses Registry;

{$R *.dfm}

const
  RegKey = 'SOFTWARE\Wiget Informatik\Gloor Produktionsplanung\';

procedure TFormKonfiguration.btnOpenClick(Sender: TObject);
begin
  if OpenDialog.Execute then
    EditDbName.Text := OpenDialog.FileName;
end;

procedure TFormKonfiguration.Button1Click(Sender: TObject);
begin
  ColorDialog.Execute;
  FormKonfiguration.Color := ColorDialog.Color;
end;

procedure TFormKonfiguration.ButtonCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TFormKonfiguration.ButtonColorExpressClick(Sender: TObject);
begin
  ColorDialog.Execute;
  ButtonColorExpress.SymbolColor := ColorDialog.Color;
  FColorExpress := ButtonColorExpress.SymbolColor;
end;

procedure TFormKonfiguration.ButtonColorNeuClick(Sender: TObject);
begin
  ColorDialog.Execute;
  ButtonColorNeu.SymbolColor := ColorDialog.Color;
  FColorNeu := ButtonColorNeu.SymbolColor;
end;

procedure TFormKonfiguration.ButtonColorReklamationenClick(Sender: TObject);
begin
  ColorDialog.Execute;
  ButtonColorReklamationen.SymbolColor := ColorDialog.Color;
  FColorReklamation := ButtonColorReklamationen.SymbolColor;
end;

procedure TFormKonfiguration.ButtonColorUeberbuchungClick(Sender: TObject);
begin
  ColorDialog.Execute;
  ButtonColorUeberbuchung.SymbolColor := ColorDialog.Color;
  FColorUeberbuchung := ButtonColorUeberbuchung.SymbolColor;
end;

procedure TFormKonfiguration.ButtonOkClick(Sender: TObject);
var
  Reg : TRegistry;
begin
  // Einstellungen in Registry speichern
  Reg := TRegistry.Create(KEY_READ);
  try
    Reg.Access := KEY_WRITE;
    Reg.OpenKey(RegKey, True);
    Reg.WriteString('DbName', EditDbName.Text);
    Reg.WriteInteger('Neu', FColorNeu);
    Reg.WriteInteger('Express', FColorExpress);
    Reg.WriteInteger('Reklamation', FColorReklamation);
    Reg.WriteInteger('Ueberbuchung', FColorUeberbuchung);
    FDbName := EditDbName.Text;
    Reg.WriteInteger('Vorschau', StrToIntDef(EditVorschau.Text, 6));
    Reg.WriteBool('LoeschenErlaubt', CheckBoxLoeschenErlaubt.Checked);
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
  Close;
end;

procedure TFormKonfiguration.ComboBoxDbClick(Sender: TObject);
begin
  if not OpenDialog.Execute then
    Exit;

  EditDbName.Text := OpenDialog.FileName;
end;

procedure TFormKonfiguration.FormCreate(Sender: TObject);
var
  Reg : TRegistry;
begin
  // FormKonfiguration.Position := poScreenCenter;
  OpenDialog.InitialDir := GetHomePath;
  Reg := TRegistry.Create;
  try
    if Reg.OpenKey(RegKey, False) then begin
      EditDbName.Text := Reg.ReadString('DbName');
      if not Reg.ValueExists('Neu') then
        Reg.WriteInteger('Neu', clGreen);
      ButtonColorNeu.SymbolColor := Reg.ReadInteger('Neu');
      FColorNeu := ButtonColorNeu.SymbolColor;
      if not Reg.ValueExists('Express') then
        Reg.WriteInteger('Express', clYellow);
      ButtonColorExpress.SymbolColor := Reg.ReadInteger('Express');
      FColorExpress := ButtonColorExpress.SymbolColor;
      if not Reg.ValueExists('Reklamation') then
        Reg.WriteInteger('Reklamation', clRed);
      ButtonColorReklamationen.SymbolColor := Reg.ReadInteger('Reklamation');
      FColorReklamation := ButtonColorReklamationen.SymbolColor;
      if not Reg.ValueExists('Ueberbuchung') then
        Reg.WriteInteger('Ueberbuchung', clWebOrangeRed);
      ButtonColorUeberbuchung.SymbolColor := Reg.ReadInteger('Ueberbuchung');
      FColorUeberbuchung := ButtonColorUeberbuchung.SymbolColor;
      if Reg.ValueExists('Vorschau') then
        FVorschau := Reg.ReadInteger('Vorschau')
      else
        FVorschau := 6;
      EditVorschau.Text := IntToStr(FVorschau);
      if not Reg.ValueExists('LoeschenErlaubt') then
        Reg.WriteBool('LoeschenErlaubt', False);
      FLoeschenErlaubt := Reg.ReadBool('LoeschenErlaubt');
      CheckBoxLoeschenErlaubt.Checked := FLoeschenErlaubt;
      if not Reg.ValueExists('Abteilung') then
        Reg.WriteString('Abteilung', '');
      FAbteilung := Reg.ReadString('Abteilung');
    end
    else begin
      FColorNeu := clGreen;
      FColorExpress := clYellow;
      FColorReklamation := clRed;
      FColorUeberbuchung := clWebOrangeRed;
    end;
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
end;

end.
