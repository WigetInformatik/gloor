object FormAuftragDrucken: TFormAuftragDrucken
  Left = 0
  Top = 0
  Caption = 'Auftr'#228'ge drucken'
  ClientHeight = 861
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    1264
    861)
  PixelsPerInch = 96
  TextHeight = 13
  object LabelAuftrag: TLabel
    Left = 24
    Top = 24
    Width = 200
    Height = 33
    Caption = 'Planung Drucken'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object LabelAbteilung: TLabel
    Left = 24
    Top = 66
    Width = 49
    Height = 13
    Caption = 'Abteilung:'
  end
  object LabelMaschine: TLabel
    Left = 208
    Top = 66
    Width = 48
    Height = 13
    Caption = 'Maschine:'
  end
  object ComboBoxAbteilung: TComboBox
    Left = 24
    Top = 85
    Width = 161
    Height = 21
    Style = csDropDownList
    TabOrder = 0
    OnChange = ComboBoxAbteilungChange
  end
  object ComboBoxMaschine: TComboBox
    Left = 208
    Top = 85
    Width = 161
    Height = 21
    Style = csDropDownList
    TabOrder = 1
    OnChange = ComboBoxMaschineChange
  end
  object ButtonOk: TButton
    Left = 568
    Top = 819
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'OK'
    TabOrder = 2
    OnClick = ButtonOkClick
    ExplicitTop = 944
  end
  object ButtonCancel: TButton
    Left = 688
    Top = 819
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'Abbrechen'
    TabOrder = 3
    OnClick = ButtonCancelClick
    ExplicitTop = 944
  end
  object AdvStringGridPlanungsliste: TAdvStringGrid
    Left = 24
    Top = 120
    Width = 1225
    Height = 684
    Cursor = crDefault
    Anchors = [akLeft, akTop, akBottom]
    DrawingStyle = gdsClassic
    ScrollBars = ssBoth
    TabOrder = 4
    HoverRowCells = [hcNormal, hcSelected]
    OnGroupCalc = AdvStringGridPlanungslisteGroupCalc
    ActiveCellFont.Charset = DEFAULT_CHARSET
    ActiveCellFont.Color = clWindowText
    ActiveCellFont.Height = -11
    ActiveCellFont.Name = 'Tahoma'
    ActiveCellFont.Style = [fsBold]
    ControlLook.FixedGradientHoverFrom = clGray
    ControlLook.FixedGradientHoverTo = clWhite
    ControlLook.FixedGradientDownFrom = clGray
    ControlLook.FixedGradientDownTo = clSilver
    ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
    ControlLook.DropDownHeader.Font.Color = clWindowText
    ControlLook.DropDownHeader.Font.Height = -11
    ControlLook.DropDownHeader.Font.Name = 'Tahoma'
    ControlLook.DropDownHeader.Font.Style = []
    ControlLook.DropDownHeader.Visible = True
    ControlLook.DropDownHeader.Buttons = <>
    ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
    ControlLook.DropDownFooter.Font.Color = clWindowText
    ControlLook.DropDownFooter.Font.Height = -11
    ControlLook.DropDownFooter.Font.Name = 'Tahoma'
    ControlLook.DropDownFooter.Font.Style = []
    ControlLook.DropDownFooter.Visible = True
    ControlLook.DropDownFooter.Buttons = <>
    Filter = <>
    FilterDropDown.Font.Charset = DEFAULT_CHARSET
    FilterDropDown.Font.Color = clWindowText
    FilterDropDown.Font.Height = -11
    FilterDropDown.Font.Name = 'Tahoma'
    FilterDropDown.Font.Style = []
    FilterDropDown.TextChecked = 'Checked'
    FilterDropDown.TextUnChecked = 'Unchecked'
    FilterDropDownClear = '(All)'
    FilterEdit.TypeNames.Strings = (
      'Starts with'
      'Ends with'
      'Contains'
      'Not contains'
      'Equal'
      'Not equal'
      'Larger than'
      'Smaller than'
      'Clear')
    FixedRowHeight = 22
    FixedFont.Charset = DEFAULT_CHARSET
    FixedFont.Color = clWindowText
    FixedFont.Height = -11
    FixedFont.Name = 'Tahoma'
    FixedFont.Style = [fsBold]
    FloatFormat = '%.2f'
    Grouping.Summary = True
    Grouping.SummaryLine = True
    HoverButtons.Buttons = <>
    HoverButtons.Position = hbLeftFromColumnLeft
    HTMLSettings.ImageFolder = 'images'
    HTMLSettings.ImageBaseName = 'img'
    PrintSettings.DateFormat = 'dd/mm/yyyy'
    PrintSettings.Font.Charset = DEFAULT_CHARSET
    PrintSettings.Font.Color = clWindowText
    PrintSettings.Font.Height = -11
    PrintSettings.Font.Name = 'Tahoma'
    PrintSettings.Font.Style = []
    PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
    PrintSettings.FixedFont.Color = clWindowText
    PrintSettings.FixedFont.Height = -11
    PrintSettings.FixedFont.Name = 'Tahoma'
    PrintSettings.FixedFont.Style = []
    PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
    PrintSettings.HeaderFont.Color = clWindowText
    PrintSettings.HeaderFont.Height = -11
    PrintSettings.HeaderFont.Name = 'Tahoma'
    PrintSettings.HeaderFont.Style = []
    PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
    PrintSettings.FooterFont.Color = clWindowText
    PrintSettings.FooterFont.Height = -11
    PrintSettings.FooterFont.Name = 'Tahoma'
    PrintSettings.FooterFont.Style = []
    PrintSettings.PageNumSep = '/'
    SearchFooter.FindNextCaption = 'Find &next'
    SearchFooter.FindPrevCaption = 'Find &previous'
    SearchFooter.Font.Charset = DEFAULT_CHARSET
    SearchFooter.Font.Color = clWindowText
    SearchFooter.Font.Height = -11
    SearchFooter.Font.Name = 'Tahoma'
    SearchFooter.Font.Style = []
    SearchFooter.HighLightCaption = 'Highlight'
    SearchFooter.HintClose = 'Close'
    SearchFooter.HintFindNext = 'Find next occurrence'
    SearchFooter.HintFindPrev = 'Find previous occurrence'
    SearchFooter.HintHighlight = 'Highlight occurrences'
    SearchFooter.MatchCaseCaption = 'Match case'
    SearchFooter.ResultFormat = '(%d of %d)'
    SortSettings.DefaultFormat = ssAutomatic
    Version = '8.3.5.2'
    ExplicitHeight = 809
    ColWidths = (
      64
      64
      64
      64
      64)
    RowHeights = (
      22
      22
      22
      22
      22
      22
      22
      22
      22
      22)
  end
  object CheckBoxAktuelleWoche: TCheckBox
    Left = 408
    Top = 41
    Width = 97
    Height = 17
    Caption = 'Aktuelle Woche'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = ChangeZeitraum
  end
  object CheckBoxNaechsteWoche: TCheckBox
    Left = 408
    Top = 64
    Width = 97
    Height = 17
    Caption = 'N'#228'chste Woche'
    TabOrder = 6
    OnClick = ChangeZeitraum
  end
  object CheckBoxUebernaechsteWoche: TCheckBox
    Left = 408
    Top = 87
    Width = 129
    Height = 17
    Caption = #220'bern'#228'chste Woche'
    TabOrder = 7
    OnClick = ChangeZeitraum
  end
  object FDPhysMSAccessDriverLink: TFDPhysMSAccessDriverLink
    Left = 56
    Top = 288
  end
  object FDConnection: TFDConnection
    Params.Strings = (
      'DriverID=MSAcc')
    LoginPrompt = False
    Left = 56
    Top = 352
  end
  object FDMSAccessService: TFDMSAccessService
    Database = 'Gloor.accdb'
    Left = 56
    Top = 424
  end
  object FDGUIxLoginDialog: TFDGUIxLoginDialog
    Provider = 'Forms'
    Left = 198
    Top = 424
  end
  object FDGUIxErrorDialog: TFDGUIxErrorDialog
    Provider = 'Forms'
    Caption = 'FireDAC Executor Error'
    Left = 198
    Top = 368
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    ScreenCursor = gcrHourGlass
    Left = 198
    Top = 304
  end
  object FDQueryAbteilung: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT * FROM Abteilungen')
    Left = 176
    Top = 128
  end
  object FDQueryMaschine: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT *  FROM Maschinen')
    Left = 176
    Top = 184
  end
  object FDQueryMaschineInAbteilung: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT *  FROM Maschinen WHERE Abteilung = :Abteilung')
    Left = 176
    Top = 240
    ParamData = <
      item
        Name = 'ABTEILUNG'
        ParamType = ptInput
      end>
  end
  object FDQueryGetAbteilungsId: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT ID FROM Abteilungen WHERE Abteilung = :Abteilung')
    Left = 310
    Top = 128
    ParamData = <
      item
        Name = 'ABTEILUNG'
        ParamType = ptInput
      end>
  end
  object FDQueryGetPlanung: TFDQuery
    Connection = FDConnection
    Left = 440
    Top = 128
  end
  object FDQueryGetMaschineId: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT ID FROM Maschinen WHERE Maschine = :Maschine')
    Left = 310
    Top = 176
    ParamData = <
      item
        Name = 'MASCHINE'
        ParamType = ptInput
      end>
  end
  object FDQueryGetKapazitaet: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT * FROM Kapazitaeten'
      'WHERE Maschine = :Maschine AND Woche = :Woche')
    Left = 454
    Top = 240
    ParamData = <
      item
        Name = 'MASCHINE'
        ParamType = ptInput
      end
      item
        Name = 'WOCHE'
        ParamType = ptInput
      end>
  end
end
