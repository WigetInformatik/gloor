unit UnitAuftragSuchen;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, AdvUtil,
  FireDAC.Phys.MSAccDef, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MSAcc,
  FireDAC.VCLUI.Wait, FireDAC.VCLUI.Error, FireDAC.VCLUI.Login,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Comp.UI,
  FireDAC.Phys.ODBCBase, Vcl.Grids, AdvObj, BaseGrid, AdvGrid;

type
  TFormAuftragSuchen = class(TForm)
    LabelKunde: TLabel;
    LabelAbNr: TLabel;
    LabelArtikelNr: TLabel;
    LabelDurchmesser: TLabel;
    LabelZaehne: TLabel;
    LabelStueck: TLabel;
    LabelAuftragsart: TLabel;
    ComboBoxKunde: TComboBox;
    EditAbNr: TEdit;
    EditArtikelNr: TEdit;
    EditDurchmesser: TEdit;
    EditZaehne: TEdit;
    EditStueck: TEdit;
    ComboBoxAuftragsart: TComboBox;
    AdvStringGridAuftrag: TAdvStringGrid;
    ButtonCancel: TButton;
    ButtonOk: TButton;
    FDPhysMSAccessDriverLink: TFDPhysMSAccessDriverLink;
    FDConnection: TFDConnection;
    FDMSAccessService: TFDMSAccessService;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    FDGUIxErrorDialog: TFDGUIxErrorDialog;
    FDGUIxLoginDialog: TFDGUIxLoginDialog;
    FDQueryKunde: TFDQuery;
    FDQueryAuftragsart: TFDQuery;
    FDQueryGetAuftrag: TFDQuery;
    ButtonSuchen: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
    procedure ButtonOkClick(Sender: TObject);
    procedure UpdateAuftrag(Sender: TObject);
    procedure AdvStringGridAuftragClickCell(Sender: TObject; ARow,
      ACol: Integer);
    procedure AdvStringGridAuftragDblClickCell(Sender: TObject; ARow,
      ACol: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ComboBoxKundeKeyPress(Sender: TObject; var Key: Char);
  private
    { Private-Deklarationen }
    FKundenListe : TStringList;
    FAuftragsart : TStringList;
    FAuftragId : Integer;
  public
    { Public-Deklarationen }
  end;

var
  FormAuftragSuchen: TFormAuftragSuchen;

implementation

uses UnitAuftrag, UnitKonfiguration, UnitMain;

{$R *.dfm}

procedure TFormAuftragSuchen.AdvStringGridAuftragClickCell(Sender: TObject;
  ARow, ACol: Integer);
begin
  if ARow > 0 then begin
    FAuftragId := StrToInt(AdvStringGridAuftrag.Cells[0, ARow]);
    AdvStringGridAuftrag.SelectRows(ARow, 1);
    ButtonOk.Visible := True;
  end;
end;

procedure TFormAuftragSuchen.AdvStringGridAuftragDblClickCell(Sender: TObject;
  ARow, ACol: Integer);
begin
  if ARow > 0 then begin
    AdvStringGridAuftrag.SelectRows(ARow, 1);
    FormAuftrag.AuftragId := StrToInt(AdvStringGridAuftrag.Cells[0, ARow]);
    FormAuftrag.ShowModal;
  end;
end;

procedure TFormAuftragSuchen.ButtonCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TFormAuftragSuchen.ButtonOkClick(Sender: TObject);
begin
  FormAuftrag.AuftragId := FAuftragId;
  FormAuftrag.ShowModal;
end;

procedure TFormAuftragSuchen.ComboBoxKundeKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key = #13) then
    UpdateAuftrag(Sender);
end;

procedure TFormAuftragSuchen.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FDConnection.Close;
end;

procedure TFormAuftragSuchen.FormCreate(Sender: TObject);
begin
  FormAuftragSuchen.Position := poScreenCenter;

  FDConnection.Params.Add('DriverID=MSAcc');
  FDConnection.Params.Add('Database=' + FormMain.DbName);
  FKundenListe := TStringList.Create;
  FAuftragsart := TStringList.Create;
end;

procedure TFormAuftragSuchen.FormShow(Sender: TObject);
begin
  FDConnection.Open;

  FAuftragId := -1;
  ButtonOk.Visible := False;
  FKundenListe.Clear;
  FAuftragsart.Clear;

  AdvStringGridAuftrag.Clear;
  ComboBoxKunde.Text := '';

  FDQueryKunde.Active := True;
  FDQueryKunde.First;
  while not FDQueryKunde.Eof do begin
    FKundenListe.Add(FDQueryKunde.FieldByName('Kunde').AsString);
    FDQueryKunde.Next;
  end;
  FDQueryKunde.Active := False;
  ComboBoxKunde.Items := FKundenListe;
  ComboBoxKunde.ItemIndex := -1;

  EditAbNr.Text := '';
  EditArtikelNr.Text := '';
  EditDurchmesser.Text := '';
  EditZaehne.Text := '';
  EditStueck.Text := '';

  FDQueryAuftragsart.Active := True;
  FDQueryAuftragsart.First;
  while not FDQueryAuftragsart.Eof do begin
    FAuftragsart.Add(FDQueryAuftragsart.FieldByName('Auftragsart').AsString);
    FDQueryAuftragsart.Next;
  end;
  FDQueryAuftragsart.Close;
  ComboBoxAuftragsart.Items := FAuftragsart;
  ComboBoxAuftragsart.ItemIndex := -1;
  UpdateAuftrag(Sender);
end;

procedure TFormAuftragSuchen.UpdateAuftrag(Sender: TObject);
var
  row : Integer;
  First : Boolean;
  SqlString : String;
begin
  First := True;
  SqlString := 'SELECT ' +
    'Auftrag.ID, AB_Nr, Artikel_Nr, Durchmesser, Zaehne, Stueck, Auftragsart, ' +
    'Kunden.Kunde FROM Auftrag LEFT JOIN Kunden ON Auftrag.Kunde = Kunden.ID ';
  if (ComboBoxKunde.Text <> '')
      or (EditAbNr.Text <> '')
      or (EditArtikelNr.Text <> '')
      or (EditDurchmesser.Text <> '')
      or (EditZaehne.Text <> '')
      or (EditStueck.Text <> '')
      or (ComboBoxAuftragsart.Text <> '') then
    SqlString := SqlString + 'WHERE ';
  if ComboBoxKunde.Text <> '' then begin
    First := False;
    SqlString := SqlString + 'Kunden.Kunde = :KundeName ';
  end;
  if EditAbNr.Text <> '' then begin
    if not First then
      SqlString := SqlString + 'AND ';
    First := False;
    SqlString := SqlString + 'AB_Nr LIKE :ABNr ';
  end;
  if EditArtikelNr.Text <> '' then begin
    if not First then
      SqlString := SqlString + 'AND ';
    First := False;
    SqlString := SqlString + 'Artikel_Nr LIKE :ArtikelNr ';
  end;
  if EditDurchmesser.Text <> '' then begin
    if not First then
      SqlString := SqlString + 'AND ';
    First := False;
    SqlString := SqlString + 'Durchmesser LIKE :Durchmesser ';
  end;
  if EditZaehne.Text <> '' then begin
    if not First then
      SqlString := SqlString + 'AND ';
    First := False;
    SqlString := SqlString + 'Zaehne LIKE :Zaehne ';
  end;
  if EditStueck.Text <> '' then begin
    if not First then
      SqlString := SqlString + 'AND ';
    First := False;
    SqlString := SqlString + 'Stueck LIKE :Stueck ';
  end;
  if ComboBoxAuftragsart.Text <> '' then begin
    if not First then
      SqlString := SqlString + 'AND ';
    First := False;
    SqlString := SqlString + 'Auftragsart = :Auftragsart ';
  end;
  SqlString := SqlString + 'ORDER BY Auftrag.ID';

  for row := 1 to AdvStringGridAuftrag.RowCount - 1 do begin
    AdvStringGridAuftrag.ClearRows(1, 1);
    AdvStringGridAuftrag.RemoveRows(1, 1);
  end;
  AdvStringGridAuftrag.Clear;
  FDQueryGetAuftrag.SQL.Clear;
  FDQueryGetAuftrag.SQL.Add(SqlString);
  if (ComboBoxKunde.Text <> '') then
    FDQueryGetAuftrag.ParamByName('KundeName').AsString := ComboBoxKunde.Text;
  if EditAbNr.Text <> '' then
    FDQueryGetAuftrag.ParamByName('ABNr').AsString := EditAbNr.Text + '%';
  if EditArtikelNr.Text <> '' then
    FDQueryGetAuftrag.ParamByName('ArtikelNr').AsString := EditArtikelNr.Text + '%';
  if EditDurchmesser.Text <> '' then
    FDQueryGetAuftrag.ParamByName('Durchmesser').AsString := EditDurchmesser.Text + '%';
  if EditZaehne.Text <> '' then
    FDQueryGetAuftrag.ParamByName('Zaehne').AsString := EditZaehne.Text + '%';
  if EditStueck.Text <> '' then
    FDQueryGetAuftrag.ParamByName('Stueck').AsString := EditStueck.Text + '%';
  if ComboBoxAuftragsart.Text <> '' then
    FDQueryGetAuftrag.ParamByName('Auftragsart').AsInteger := FAuftragsart.IndexOf(ComboBoxAuftragsart.Text) + 1;
  FDQueryGetAuftrag.Open;

  AdvStringGridAuftrag.Cells[0, 0] := 'ID';
  AdvStringGridAuftrag.Cells[1, 0] := 'Kunde';
  AdvStringGridAuftrag.Cells[2, 0] := 'AB-Nr';
  AdvStringGridAuftrag.Cells[3, 0] := 'Artikel-Nr';
  AdvStringGridAuftrag.Cells[4, 0] := 'Durchmesser';
  AdvStringGridAuftrag.Cells[5, 0] := 'Z�hne';
  AdvStringGridAuftrag.Cells[6, 0] := 'St�ck';
  AdvStringGridAuftrag.Cells[7, 0] := 'Auftragsart';
  AdvStringGridAuftrag.ColCount := 8;

  FDQueryGetAuftrag.First;
  row := 1;
  try
    AdvStringGridAuftrag.BeginUpdate;
    while not FDQueryGetAuftrag.Eof do begin
      AdvStringGridAuftrag.AddRow;
      AdvStringGridAuftrag.Cells[0, row] := FDQueryGetAuftrag.FieldByName('ID').AsString;
      AdvStringGridAuftrag.Cells[1, row] := FDQueryGetAuftrag.FieldByName('Kunde').AsString;
      AdvStringGridAuftrag.Cells[2, row] := FDQueryGetAuftrag.FieldByName('AB_Nr').AsString;
      AdvStringGridAuftrag.Cells[3, row] := FDQueryGetAuftrag.FieldByName('Artikel_Nr').AsString;
      AdvStringGridAuftrag.Cells[4, row] := FDQueryGetAuftrag.FieldByName('Durchmesser').AsString;
      AdvStringGridAuftrag.Cells[5, row] := FDQueryGetAuftrag.FieldByName('Zaehne').AsString;
      AdvStringGridAuftrag.Cells[6, row] := FDQueryGetAuftrag.FieldByName('Stueck').AsString;
      AdvStringGridAuftrag.Cells[7, row] := FAuftragsart[FDQueryGetAuftrag.FieldByName('Auftragsart').AsInteger-1];

      if FDQueryGetAuftrag.FieldByName('Auftragsart').AsInteger = FAuftragsart.IndexOf('Neues Werkzeug') + 1 then
        AdvStringGridAuftrag.RowColor[row] := FormKonfiguration.ColorNeu;
      if FDQueryGetAuftrag.FieldByName('Auftragsart').AsInteger = FAuftragsart.IndexOf('Express') + 1 then
        AdvStringGridAuftrag.RowColor[row] := FormKonfiguration.ColorExpress;
      if FDQueryGetAuftrag.FieldByName('Auftragsart').AsInteger = FAuftragsart.IndexOf('Reklamation') + 1 then
        AdvStringGridAuftrag.RowColor[row] := FormKonfiguration.ColorReklamation;

      FDQueryGetAuftrag.Next;
      row := row + 1;
    end;
  finally
    AdvStringGridAuftrag.EndUpdate;
  end;
  FDQueryGetAuftrag.Close;
  AdvStringGridAuftrag.AutoSizeColumns(true, 4);
  // AdvStringGridAuftrag.RowCount := row;
end;

end.
