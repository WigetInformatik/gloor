program Fertigungsplanung;

uses
  Vcl.Forms,
  UnitMain in 'UnitMain.pas' {FormMain},
  UnitAuftrag in 'UnitAuftrag.pas' {FormAuftrag},
  UnitAuftragSuchen in 'UnitAuftragSuchen.pas' {FormAuftragSuchen},
  UnitAuftragDrucken in 'UnitAuftragDrucken.pas' {FormAuftragDrucken},
  UnitKapazitaetDefinieren in 'UnitKapazitaetDefinieren.pas' {FormKapazitaetDefinieren},
  UnitKonfiguration in 'UnitKonfiguration.pas' {FormKonfiguration};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormAuftrag, FormAuftrag);
  Application.CreateForm(TFormAuftragSuchen, FormAuftragSuchen);
  Application.CreateForm(TFormAuftragDrucken, FormAuftragDrucken);
  Application.CreateForm(TFormKapazitaetDefinieren, FormKapazitaetDefinieren);
  Application.CreateForm(TFormKonfiguration, FormKonfiguration);
  Application.Run;
end.
