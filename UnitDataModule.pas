unit UnitDataModule;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Phys.MSAccDef, FireDAC.Stan.Intf,
  FireDAC.Phys, FireDAC.Phys.ODBCBase, FireDAC.Phys.MSAcc, FireDAC.UI.Intf,
  FireDAC.VCLUI.Wait, FireDAC.VCLUI.Error, FireDAC.Stan.Error,
  FireDAC.VCLUI.Login, FireDAC.Stan.Def, FireDAC.Stan.Option, FireDAC.Phys.Intf,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, Data.DB, FireDAC.Comp.Client,
  FireDAC.Comp.UI, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet;

type
  TDataModuleGloor = class(TDataModule)
    FDPhysMSAccessDriverLink: TFDPhysMSAccessDriverLink;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    FDGUIxErrorDialog: TFDGUIxErrorDialog;
    FDGUIxLoginDialog: TFDGUIxLoginDialog;
    FDMSAccessService: TFDMSAccessService;
    FDConnection: TFDConnection;
    FDQueryAbteilungen: TFDQuery;
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    function GetAbteilungen : TStringList;
  end;

var
  DataModuleGloor: TDataModuleGloor;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDataModuleGloor }

function TDataModuleGloor.GetAbteilungen: TStringList;
var

begin
  FDQueryAbteilungen.Active := True;
  FDQueryAbteilungen.Active := False;
end;

end.
