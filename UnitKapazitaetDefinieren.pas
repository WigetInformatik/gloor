unit UnitKapazitaetDefinieren;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Phys.MSAccDef,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.MSAcc, FireDAC.VCLUI.Wait, FireDAC.VCLUI.Error,
  FireDAC.VCLUI.Login, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Comp.UI, FireDAC.Phys.ODBCBase, Vcl.StdCtrls;

type
  TFormKapazitaetDefinieren = class(TForm)
    LabelKapazitaet: TLabel;
    ComboBoxMaschine: TComboBox;
    LabelMaschine: TLabel;
    FDPhysMSAccessDriverLink: TFDPhysMSAccessDriverLink;
    FDConnection: TFDConnection;
    FDMSAccessService: TFDMSAccessService;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    FDGUIxErrorDialog: TFDGUIxErrorDialog;
    FDGUIxLoginDialog: TFDGUIxLoginDialog;
    FDQueryMaschine: TFDQuery;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    EditWoche1: TEdit;
    EditWoche2: TEdit;
    EditWoche3: TEdit;
    EditWoche4: TEdit;
    EditWoche5: TEdit;
    EditWoche6: TEdit;
    EditWoche7: TEdit;
    EditWoche8: TEdit;
    EditWoche9: TEdit;
    EditWoche10: TEdit;
    EditWoche11: TEdit;
    EditWoche12: TEdit;
    Label13: TLabel;
    EditWoche13: TEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    EditWoche14: TEdit;
    EditWoche15: TEdit;
    EditWoche16: TEdit;
    EditWoche17: TEdit;
    EditWoche18: TEdit;
    EditWoche19: TEdit;
    EditWoche20: TEdit;
    EditWoche21: TEdit;
    EditWoche22: TEdit;
    EditWoche23: TEdit;
    EditWoche24: TEdit;
    EditWoche25: TEdit;
    Label26: TLabel;
    EditWoche26: TEdit;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    EditWoche27: TEdit;
    EditWoche28: TEdit;
    EditWoche29: TEdit;
    EditWoche30: TEdit;
    EditWoche31: TEdit;
    EditWoche32: TEdit;
    EditWoche33: TEdit;
    EditWoche34: TEdit;
    EditWoche35: TEdit;
    EditWoche36: TEdit;
    EditWoche37: TEdit;
    EditWoche38: TEdit;
    Label39: TLabel;
    EditWoche39: TEdit;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    EditWoche40: TEdit;
    EditWoche41: TEdit;
    EditWoche42: TEdit;
    EditWoche43: TEdit;
    EditWoche44: TEdit;
    EditWoche45: TEdit;
    EditWoche46: TEdit;
    EditWoche47: TEdit;
    EditWoche48: TEdit;
    EditWoche49: TEdit;
    EditWoche50: TEdit;
    EditWoche51: TEdit;
    Label52: TLabel;
    EditWoche52: TEdit;
    ButtonOk: TButton;
    ButtonCancel: TButton;
    FDQueryGetMaschineId: TFDQuery;
    FDQueryGetKapazitaet: TFDQuery;
    FDQueryDeleteKapazitaet: TFDQuery;
    FDQueryAddKapazitaet: TFDQuery;
    LabelAbteilung: TLabel;
    ComboBoxAbteilung: TComboBox;
    FDQueryAbteilung: TFDQuery;
    FDQueryMaschineInAbteilung: TFDQuery;
    FDQueryGetAbteilungsId: TFDQuery;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
    procedure ButtonOkClick(Sender: TObject);
    procedure ComboBoxMaschineChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ComboBoxAbteilungChange(Sender: TObject);
  private
    { Private-Deklarationen }
    FAbteilungsListe : TStringList;
    FMaschinenListe : TStringList;
    FEditWocheList : array [1..52] of TEdit;
    function GetMaschineId (Maschine : String) : Integer;
    function GetAbteilungId (Abteilung : String) : Integer;
    procedure UpdateGui;
    procedure SaveToDatabase;
  public
    { Public-Deklarationen }
  end;

var
  FormKapazitaetDefinieren: TFormKapazitaetDefinieren;

implementation

{$R *.dfm}

uses UnitMain, UnitKonfiguration;

procedure TFormKapazitaetDefinieren.ButtonCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TFormKapazitaetDefinieren.ButtonOkClick(Sender: TObject);
begin
  SaveToDatabase;
  Close;
end;

procedure TFormKapazitaetDefinieren.ComboBoxAbteilungChange(Sender: TObject);
begin
  // Update Maschine
  FMaschinenListe.Clear;

  if ComboBoxAbteilung.ItemIndex > 1 then begin
    FDQueryMaschineInAbteilung.ParamByName('Abteilung').AsInteger := GetAbteilungId (ComboBoxAbteilung.Text);
    FDQueryMaschineInAbteilung.Active := True;
    FDQueryMaschineInAbteilung.First;
    while not FDQueryMaschineInAbteilung.Eof do begin
      FMaschinenListe.Add(FDQueryMaschineInAbteilung.FieldByName('Maschine').AsString);
      FDQueryMaschineInAbteilung.Next;
    end;
    FDQueryMaschineInAbteilung.Close;
  end
  else begin
    FDQueryMaschine.Active := True;
    FDQueryMaschine.First;
    while not FDQueryMaschine.Eof do begin
      FMaschinenListe.Add(FDQueryMaschine.FieldByName('Maschine').AsString);
      FDQueryMaschine.Next;
    end;
    FDQueryMaschine.Close;
  end;

  ComboBoxmaschine.Items := FMaschinenListe;
  ComboBoxmaschine.ItemIndex := -1;

  ComboBoxMaschineChange(Sender);
end;

procedure TFormKapazitaetDefinieren.ComboBoxMaschineChange(Sender: TObject);
begin
  UpdateGui;
end;

procedure TFormKapazitaetDefinieren.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FDConnection.Close;
end;

procedure TFormKapazitaetDefinieren.FormCreate(Sender: TObject);
begin
  FormKapazitaetDefinieren.Position := poScreenCenter;

  FDConnection.Params.Add('DriverID=MSAcc');
  FDConnection.Params.Add('Database=' + FormMain.DbName);

  FAbteilungsListe := TStringList.Create;
  FMaschinenListe := TStringList.Create;

  FEditWocheList[1] := EditWoche1;
  FEditWocheList[2] := EditWoche2;
  FEditWocheList[3] := EditWoche3;
  FEditWocheList[4] := EditWoche4;
  FEditWocheList[5] := EditWoche5;
  FEditWocheList[6] := EditWoche6;
  FEditWocheList[7] := EditWoche7;
  FEditWocheList[8] := EditWoche8;
  FEditWocheList[9] := EditWoche9;
  FEditWocheList[10] := EditWoche10;
  FEditWocheList[11] := EditWoche11;
  FEditWocheList[12] := EditWoche12;
  FEditWocheList[13] := EditWoche13;
  FEditWocheList[14] := EditWoche14;
  FEditWocheList[15] := EditWoche15;
  FEditWocheList[16] := EditWoche16;
  FEditWocheList[17] := EditWoche17;
  FEditWocheList[18] := EditWoche18;
  FEditWocheList[19] := EditWoche19;
  FEditWocheList[20] := EditWoche20;
  FEditWocheList[21] := EditWoche21;
  FEditWocheList[22] := EditWoche22;
  FEditWocheList[23] := EditWoche23;
  FEditWocheList[24] := EditWoche24;
  FEditWocheList[25] := EditWoche25;
  FEditWocheList[26] := EditWoche26;
  FEditWocheList[27] := EditWoche27;
  FEditWocheList[28] := EditWoche28;
  FEditWocheList[29] := EditWoche29;
  FEditWocheList[30] := EditWoche30;
  FEditWocheList[31] := EditWoche31;
  FEditWocheList[32] := EditWoche32;
  FEditWocheList[33] := EditWoche33;
  FEditWocheList[34] := EditWoche34;
  FEditWocheList[35] := EditWoche35;
  FEditWocheList[36] := EditWoche36;
  FEditWocheList[37] := EditWoche37;
  FEditWocheList[38] := EditWoche38;
  FEditWocheList[39] := EditWoche39;
  FEditWocheList[40] := EditWoche40;
  FEditWocheList[41] := EditWoche41;
  FEditWocheList[42] := EditWoche42;
  FEditWocheList[43] := EditWoche43;
  FEditWocheList[44] := EditWoche44;
  FEditWocheList[45] := EditWoche45;
  FEditWocheList[46] := EditWoche46;
  FEditWocheList[47] := EditWoche47;
  FEditWocheList[48] := EditWoche48;
  FEditWocheList[49] := EditWoche49;
  FEditWocheList[50] := EditWoche50;
  FEditWocheList[51] := EditWoche51;
  FEditWocheList[52] := EditWoche52;
end;

procedure TFormKapazitaetDefinieren.FormShow(Sender: TObject);
var
  Woche : Integer;
begin
  FDConnection.Open;

  FAbteilungsListe.Clear;
  FDQueryAbteilung.Active := True;
  FDQueryAbteilung.First;
  while not FDQueryAbteilung.Eof do begin
    FAbteilungsListe.Add(FDQueryAbteilung.FieldByName('Abteilung').AsString);
    FDQueryAbteilung.Next;
  end;
  FDQueryAbteilung.Close;
  ComboBoxAbteilung.Items := FAbteilungsListe;
  ComboBoxAbteilung.ItemIndex := FAbteilungsListe.IndexOf(FormKonfiguration.Abteilung);

  ComboBoxAbteilungChange(Sender);

  for Woche := 1 to 52 do
    FEditWocheList[Woche].Text := '';
end;

function TFormKapazitaetDefinieren.GetAbteilungId(Abteilung: String): Integer;
begin
  FDQueryGetAbteilungsId.ParamByName('Abteilung').AsString := Abteilung;
  FDQueryGetAbteilungsId.Open;
  FDQueryGetAbteilungsId.First;
  Result := FDQueryGetAbteilungsId.FieldByName('ID').AsInteger;
  FDQueryGetAbteilungsId.Close;
end;

function TFormKapazitaetDefinieren.GetMaschineId(Maschine: String): Integer;
begin
  FDQueryGetMaschineId.ParamByName('Maschine').AsString := Maschine;
  FDQueryGetMaschineId.Open;
  FDQueryGetMaschineId.First;
  Result := FDQueryGetMaschineId.FieldByName('ID').AsInteger;
  FDQueryGetMaschineId.Close;
end;

procedure TFormKapazitaetDefinieren.SaveToDatabase;
var
  MaschineId : Integer;
  Woche: Integer;
begin
  MaschineId := GetMaschineId (ComboBoxMaschine.Text);

  FDQueryDeleteKapazitaet.ParamByName('Maschine').AsInteger := MaschineId;
  FDQueryDeleteKapazitaet.ExecSQL;

  FDQueryAddKapazitaet.ParamByName('Maschine').AsInteger := MaschineId;
  for Woche := 1 to 52 do begin
    FDQueryAddKapazitaet.ParamByName('Woche').AsInteger := Woche;
    FDQueryAddKapazitaet.ParamByName('Kapazitaet').AsInteger := StrToIntDef(FEditWocheList[Woche].Text, 0);
    FDQueryAddKapazitaet.ExecSQL;
  end;
end;

procedure TFormKapazitaetDefinieren.UpdateGui;
var
  MaschineId : Integer;
  Woche: Integer;
begin
  for Woche := 1 to 52 do
    FEditWocheList[Woche].Text := '';

  MaschineId := GetMaschineId (ComboBoxMaschine.Text);

  FDQueryGetKapazitaet.ParamByName('Maschine').AsInteger := MaschineId;
  FDQueryGetKapazitaet.Open;
  FDQueryGetKapazitaet.First;
  while not FDQueryGetKapazitaet.Eof do begin
    if FDQueryGetKapazitaet.FieldByName('Kapazitaet').AsInteger > 0 then
      FEditWocheList[FDQueryGetKapazitaet.FieldByName('Woche').AsInteger].Text :=
          FDQueryGetKapazitaet.FieldByName('Kapazitaet').AsString;
    FDQueryGetKapazitaet.Next;
  end;
  FDQueryGetKapazitaet.Close;
end;

end.
