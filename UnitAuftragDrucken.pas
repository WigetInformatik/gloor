unit UnitAuftragDrucken;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Phys.MSAccDef,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.MSAcc, FireDAC.VCLUI.Login, FireDAC.VCLUI.Error,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.ODBCBase, Data.DB,
  FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, AdvUtil, Vcl.Grids, AdvObj, BaseGrid,
  AdvGrid;

type
  TFormAuftragDrucken = class(TForm)
    LabelAuftrag: TLabel;
    ComboBoxAbteilung: TComboBox;
    ComboBoxMaschine: TComboBox;
    LabelAbteilung: TLabel;
    LabelMaschine: TLabel;
    FDPhysMSAccessDriverLink: TFDPhysMSAccessDriverLink;
    FDConnection: TFDConnection;
    FDMSAccessService: TFDMSAccessService;
    FDGUIxLoginDialog: TFDGUIxLoginDialog;
    FDGUIxErrorDialog: TFDGUIxErrorDialog;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    FDQueryAbteilung: TFDQuery;
    FDQueryMaschine: TFDQuery;
    ButtonOk: TButton;
    ButtonCancel: TButton;
    FDQueryMaschineInAbteilung: TFDQuery;
    FDQueryGetAbteilungsId: TFDQuery;
    AdvStringGridPlanungsliste: TAdvStringGrid;
    FDQueryGetPlanung: TFDQuery;
    FDQueryGetMaschineId: TFDQuery;
    FDQueryGetKapazitaet: TFDQuery;
    CheckBoxAktuelleWoche: TCheckBox;
    CheckBoxNaechsteWoche: TCheckBox;
    CheckBoxUebernaechsteWoche: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
    procedure ButtonOkClick(Sender: TObject);
    procedure ComboBoxAbteilungChange(Sender: TObject);
    procedure ComboBoxMaschineChange(Sender: TObject);
    procedure AdvStringGridPlanungslisteGroupCalc(Sender: TObject; ACol,
      FromRow, ToRow: Integer; var Res: Double);
    procedure ChangeZeitraum(Sender: TObject);
  private
    { Private-Deklarationen }
    FAbteilungsListe : TStringList;
    FMaschinenListe : TStringList;
    FMaschineId : Integer;
    function GetAbteilungId (Abteilung : String) : Integer;
    function GetMaschineId (Maschine : String) : Integer;
    procedure UpdatePlanungsListe;
  public
    { Public-Deklarationen }
  end;

var
  FormAuftragDrucken: TFormAuftragDrucken;

implementation

{$R *.dfm}

uses DateUtils, UnitMain, UnitKonfiguration;

procedure TFormAuftragDrucken.AdvStringGridPlanungslisteGroupCalc(
  Sender: TObject; ACol, FromRow, ToRow: Integer; var Res: Double);
var
  Kapazitaet,
  i : Integer;
begin
  FDQueryGetKapazitaet.ParamByName('Maschine').AsInteger := FMaschineId;
  FDQueryGetKapazitaet.ParamByName('Woche').AsInteger := StrToIntDef (AdvStringGridPlanungsliste.Cells[1, FromRow-1], 0);
  FDQueryGetKapazitaet.Open;
  FDQueryGetKapazitaet.First;
  Kapazitaet := FDQueryGetKapazitaet.FieldByName('Kapazitaet').AsInteger;
  FDQueryGetKapazitaet.Close;

  Res := 0.0;
  try
    for i := FromRow to ToRow do
      try
        Res := Res + AdvStringGridPlanungsliste.Floats[ACol, i];
      except
        // Catch error if there is no float value
      end;
    AdvStringGridPlanungsliste.Cells[1, FromRow-1] := 'Woche ' + AdvStringGridPlanungsliste.Cells[1, FromRow-1] + ': '
        + FloatToStr(Res) + ' / ' + IntToStr(Kapazitaet);
    if Res > Kapazitaet then
      AdvStringGridPlanungsliste.Colors[1, FromRow-1] := clRed;
  except
    exit;
  end;
end;

procedure TFormAuftragDrucken.ButtonCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TFormAuftragDrucken.ButtonOkClick(Sender: TObject);
begin
  // Drucke Auftragsliste
  AdvStringGridPlanungsliste.Print;
end;

procedure TFormAuftragDrucken.ChangeZeitraum(Sender: TObject);
begin
  UpdatePlanungsListe;
end;

procedure TFormAuftragDrucken.ComboBoxAbteilungChange(Sender: TObject);
begin
  // Update Maschine
  FMaschinenListe.Clear;
  FMaschinenListe.Add('Alle');

  if ComboBoxAbteilung.ItemIndex > 1 then begin
    FDQueryMaschineInAbteilung.ParamByName('Abteilung').AsInteger := GetAbteilungId (ComboBoxAbteilung.Text);
    FDQueryMaschineInAbteilung.Active := True;
    FDQueryMaschineInAbteilung.First;
    while not FDQueryMaschineInAbteilung.Eof do begin
      FMaschinenListe.Add(FDQueryMaschineInAbteilung.FieldByName('Maschine').AsString);
      FDQueryMaschineInAbteilung.Next;
    end;
    FDQueryMaschineInAbteilung.Close;
  end
  else begin
    FDQueryMaschine.Active := True;
    FDQueryMaschine.First;
    while not FDQueryMaschine.Eof do begin
      FMaschinenListe.Add(FDQueryMaschine.FieldByName('Maschine').AsString);
      FDQueryMaschine.Next;
    end;
    FDQueryMaschine.Close;
  end;

  ComboBoxmaschine.Items := FMaschinenListe;
  ComboBoxmaschine.ItemIndex := 0;

  ComboBoxMaschineChange(Sender);
end;

procedure TFormAuftragDrucken.ComboBoxMaschineChange(Sender: TObject);
begin
  UpdatePlanungsListe;
end;

procedure TFormAuftragDrucken.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FDConnection.Close;
end;

procedure TFormAuftragDrucken.FormCreate(Sender: TObject);
begin
  FormAuftragDrucken.Position := poScreenCenter;

  FDConnection.Params.Add('DriverID=MSAcc');
  FDConnection.Params.Add('Database=' + FormMain.DbName);
  FAbteilungsListe := TStringList.Create;
  FMaschinenListe := TStringList.Create;
end;

procedure TFormAuftragDrucken.FormShow(Sender: TObject);
var
  row : Integer;
begin
  FDConnection.Open;
  FAbteilungsListe.Clear;
  FMaschinenListe.Clear;

  FDQueryAbteilung.Active := True;
  FDQueryAbteilung.First;
  while not FDQueryAbteilung.Eof do begin
    FAbteilungsListe.Add(FDQueryAbteilung.FieldByName('Abteilung').AsString);
    FDQueryAbteilung.Next;
  end;
  FDQueryAbteilung.Active := False;

  ComboBoxAbteilung.Items := FAbteilungsListe;
  ComboBoxAbteilung.ItemIndex := FAbteilungsListe.IndexOf(FormKonfiguration.Abteilung);

  ComboBoxAbteilungChange(Sender);
end;

function TFormAuftragDrucken.GetAbteilungId(Abteilung: String): Integer;
begin
  FDQueryGetAbteilungsId.ParamByName('Abteilung').AsString := Abteilung;
  FDQueryGetAbteilungsId.Open;
  FDQueryGetAbteilungsId.First;
  Result := FDQueryGetAbteilungsId.FieldByName('ID').AsInteger;
  FDQueryGetAbteilungsId.Close;
end;

function TFormAuftragDrucken.GetMaschineId(Maschine: String): Integer;
begin
  FDQueryGetMaschineId.ParamByName('Maschine').AsString := Maschine;
  FDQueryGetMaschineId.Open;
  FDQueryGetMaschineId.First;
  Result := FDQueryGetMaschineId.FieldByName('ID').AsInteger;
  FDQueryGetMaschineId.Close;
end;

procedure TFormAuftragDrucken.UpdatePlanungsListe;
var
  AnzWochen,
  row : Integer;
  WocheText,
  SqlText : String;
begin
  AdvStringGridPlanungsliste.UnGroup;
  for row := 1 to AdvStringGridPlanungsliste.RowCount - 1 do begin
    AdvStringGridPlanungsliste.ClearRows(1, 1);
    AdvStringGridPlanungsliste.RemoveRows(1, 1);
  end;
  AdvStringGridPlanungsliste.Clear;

  AdvStringGridPlanungsliste.Cells[1, 0] := 'Woche';
  AdvStringGridPlanungsliste.Cells[2, 0] := 'ID';
  AdvStringGridPlanungsliste.Cells[3, 0] := 'Kunde';
  AdvStringGridPlanungsliste.Cells[4, 0] := 'AB-Nr';
  AdvStringGridPlanungsliste.Cells[5, 0] := 'Artikel-Nr';
  AdvStringGridPlanungsliste.Cells[6, 0] := 'Durchmesser';
  AdvStringGridPlanungsliste.Cells[7, 0] := 'Z�hne';
  AdvStringGridPlanungsliste.Cells[8, 0] := 'St�ck';
  AdvStringGridPlanungsliste.Cells[9, 0] := 'Auftragsart';
  AdvStringGridPlanungsliste.Cells[10, 0] := 'Abteilung';
  AdvStringGridPlanungsliste.Cells[11, 0] := 'Maschine';
  AdvStringGridPlanungsliste.Cells[12, 0] := 'Kommentar';
  AdvStringGridPlanungsliste.Cells[13, 0] := 'Vorgabe';
  AdvStringGridPlanungsliste.ColCount := 14;

  AdvStringGridPlanungsliste.Grouping.MergeHeader := True;

  AnzWochen := 0;
  if CheckBoxAktuelleWoche.Checked then
    Inc (AnzWochen);
  if CheckBoxNaechsteWoche.Checked then
    Inc (AnzWochen);
  if CheckBoxUebernaechsteWoche.Checked then
    Inc (AnzWochen);

  case AnzWochen of
    0: WocheText := '';
    1: WocheText := 'AND (Woche < :Woche OR Woche = :Woche1)';
    2: WocheText := 'AND (Woche < :Woche OR Woche = :Woche1 OR Woche = :Woche2) ';
    3: WocheText := 'AND Woche <= :Woche ';
  end;

  if ComboBoxMaschine.ItemIndex > 0 then begin
    // Maschine selektiert
    SqlText := 'SELECT Planung.Kommentar, Vorgabe, Woche, Erledigt, Datum, ' +
                   'Abteilungen.Abteilung , Maschinen.Maschine, ' +
                   'Auftrag.ID, AB_Nr, Artikel_Nr, Durchmesser, Zaehne, Stueck, ' +
                   'Kunden.Kunde, Auftragsart.Auftragsart ' +
                   'FROM ((((Planung LEFT JOIN Abteilungen ON Planung.Abteilung = Abteilungen.ID) ' +
                   'LEFT JOIN Maschinen ON Planung.Maschine = Maschinen.ID) ' +
                   'RIGHT JOIN Auftrag ON Planung.Auftrag = Auftrag.ID) ' +
                   'LEFT JOIN Kunden ON Auftrag.Kunde = Kunden.ID) ' +
                   'LEFT JOIN Auftragsart ON Auftrag.Auftragsart = Auftragsart.ID ' +
               'WHERE Planung.Erledigt = FALSE ' +
                   'AND Planung.Maschine = :Maschine ' +
                   WocheText +
               'ORDER BY Woche';
    FDQueryGetPlanung.SQL.Text := SqlText;
    FMaschineId := GetMaschineId(ComboBoxMaschine.Text);
    FDQueryGetPlanung.ParamByName('Maschine').AsInteger := FMaschineId;
  end
  else if (ComboBoxAbteilung.Text <> '') and (ComboBoxAbteilung.Text <> 'Alle') then begin
    // Abteilung selektiert
    SqlText := 'SELECT Planung.Kommentar, Vorgabe, Woche, Erledigt, Datum, ' +
                   'Abteilungen.Abteilung , Maschinen.Maschine, ' +
                   'Auftrag.ID, AB_Nr, Artikel_Nr, Durchmesser, Zaehne, Stueck, ' +
                   'Kunden.Kunde, Auftragsart.Auftragsart ' +
                   'FROM ((((Planung LEFT JOIN Abteilungen ON Planung.Abteilung = Abteilungen.ID) ' +
                   'LEFT JOIN Maschinen ON Planung.Maschine = Maschinen.ID) ' +
                   'RIGHT JOIN Auftrag ON Planung.Auftrag = Auftrag.ID) ' +
                   'LEFT JOIN Kunden ON Auftrag.Kunde = Kunden.ID) ' +
                   'LEFT JOIN Auftragsart ON Auftrag.Auftragsart = Auftragsart.ID ' +
               'WHERE Planung.Erledigt = FALSE ' +
                   'AND Planung.Abteilung = :Abteilung ' +
                   WocheText +
               'ORDER BY Woche';
    FDQueryGetPlanung.SQL.Text := SqlText;
    FDQueryGetPlanung.ParamByName('Abteilung').AsInteger := GetAbteilungId(ComboBoxAbteilung.Text);
  end;
  case AnzWochen of
    1: begin
         FDQueryGetPlanung.ParamByName('Woche').AsInteger := WeekOfTheYear(Now);
         if CheckBoxAktuelleWoche.Checked then
           FDQueryGetPlanung.ParamByName('Woche1').AsInteger := WeekOfTheYear(Now)
         else if CheckBoxNaechsteWoche.Checked then
           FDQueryGetPlanung.ParamByName('Woche1').AsInteger := WeekOfTheYear(Now) + 1
         else if CheckBoxUebernaechsteWoche.Checked then
           FDQueryGetPlanung.ParamByName('Woche1').AsInteger := WeekOfTheYear(Now) + 2;
       end;
    2: if CheckBoxAktuelleWoche.Checked then begin
         FDQueryGetPlanung.ParamByName('Woche').AsInteger := WeekOfTheYear(Now);
         FDQueryGetPlanung.ParamByName('Woche1').AsInteger := WeekOfTheYear(Now);
         if CheckBoxNaechsteWoche.Checked then
           FDQueryGetPlanung.ParamByName('Woche2').AsInteger := WeekOfTheYear(Now) + 1
         else if CheckBoxUebernaechsteWoche.Checked then
           FDQueryGetPlanung.ParamByName('Woche2').AsInteger := WeekOfTheYear(Now) + 2;
       end
       else begin
         FDQueryGetPlanung.ParamByName('Woche1').AsInteger := WeekOfTheYear(Now) + 1;
         FDQueryGetPlanung.ParamByName('Woche2').AsInteger := WeekOfTheYear(Now) + 2;
       end;
    3: FDQueryGetPlanung.ParamByName('Woche').AsInteger := WeekOfTheYear(Now) + 2;
  end;


  FDQueryGetPlanung.Open;
  FDQueryGetPlanung.First;
  row := 1;

  try
    AdvStringGridPlanungsliste.BeginUpdate;
    while not FDQueryGetPlanung.Eof do begin
      AdvStringGridPlanungsliste.AddRow;
      if FDQueryGetPlanung.FieldByName('Woche').AsInteger > 0 then
        AdvStringGridPlanungsliste.Cells[1, row] := FDQueryGetPlanung.FieldByName('Woche').AsString;
      AdvStringGridPlanungsliste.Cells[2, row] := FDQueryGetPlanung.FieldByName('ID').AsString;
      AdvStringGridPlanungsliste.Cells[3, row] := FDQueryGetPlanung.FieldByName('Kunde').AsString;
      AdvStringGridPlanungsliste.Cells[4, row] := FDQueryGetPlanung.FieldByName('AB_Nr').AsString;
      AdvStringGridPlanungsliste.Cells[5, row] := FDQueryGetPlanung.FieldByName('Artikel_Nr').AsString;
      AdvStringGridPlanungsliste.Cells[6, row] := FDQueryGetPlanung.FieldByName('Durchmesser').AsString;
      AdvStringGridPlanungsliste.Cells[7, row] := FDQueryGetPlanung.FieldByName('Zaehne').AsString;
      AdvStringGridPlanungsliste.Cells[8, row] := FDQueryGetPlanung.FieldByName('Stueck').AsString;
      AdvStringGridPlanungsliste.Cells[9, row] := FDQueryGetPlanung.FieldByName('Auftragsart').AsString;
      AdvStringGridPlanungsliste.Cells[10, row] := FDQueryGetPlanung.FieldByName('Abteilung').AsString;
      AdvStringGridPlanungsliste.Cells[11, row] := FDQueryGetPlanung.FieldByName('Maschine').AsString;
      AdvStringGridPlanungsliste.Cells[12, row] := FDQueryGetPlanung.FieldByName('Kommentar').AsString;
      if FDQueryGetPlanung.FieldByName('Vorgabe').AsInteger > 0 then
        AdvStringGridPlanungsliste.Cells[13, row] := FDQueryGetPlanung.FieldByName('Vorgabe').AsString;
      row := row + 1;
      FDQueryGetPlanung.Next;
    end;
  finally
    AdvStringGridPlanungsliste.EndUpdate;
  end;
  FDQueryGetPlanung.Close;

  AdvStringGridPlanungsliste.Group(1);
  AdvStringGridPlanungsliste.GroupCustomCalc(12);
  AdvStringGridPlanungsliste.AutoSizeColumns(true, 4);
end;

end.
