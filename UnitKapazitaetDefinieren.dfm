object FormKapazitaetDefinieren: TFormKapazitaetDefinieren
  Left = 0
  Top = 0
  Caption = 'Kapazit'#228't definieren'
  ClientHeight = 529
  ClientWidth = 566
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LabelKapazitaet: TLabel
    Left = 24
    Top = 24
    Width = 109
    Height = 33
    Caption = 'Kapazit'#228't'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object LabelMaschine: TLabel
    Left = 210
    Top = 72
    Width = 48
    Height = 13
    Caption = 'Maschine:'
  end
  object Label1: TLabel
    Left = 24
    Top = 136
    Width = 46
    Height = 13
    Caption = 'Woche 1:'
  end
  object Label2: TLabel
    Left = 24
    Top = 159
    Width = 46
    Height = 13
    Caption = 'Woche 2:'
  end
  object Label3: TLabel
    Left = 24
    Top = 182
    Width = 46
    Height = 13
    Caption = 'Woche 3:'
  end
  object Label4: TLabel
    Left = 24
    Top = 206
    Width = 46
    Height = 13
    Caption = 'Woche 4:'
  end
  object Label5: TLabel
    Left = 24
    Top = 229
    Width = 46
    Height = 13
    Caption = 'Woche 5:'
  end
  object Label6: TLabel
    Left = 24
    Top = 253
    Width = 46
    Height = 13
    Caption = 'Woche 6:'
  end
  object Label7: TLabel
    Left = 24
    Top = 276
    Width = 46
    Height = 13
    Caption = 'Woche 7:'
  end
  object Label8: TLabel
    Left = 24
    Top = 299
    Width = 46
    Height = 13
    Caption = 'Woche 8:'
  end
  object Label9: TLabel
    Left = 24
    Top = 323
    Width = 46
    Height = 13
    Caption = 'Woche 9:'
  end
  object Label10: TLabel
    Left = 24
    Top = 346
    Width = 52
    Height = 13
    Caption = 'Woche 10:'
  end
  object Label11: TLabel
    Left = 24
    Top = 370
    Width = 52
    Height = 13
    Caption = 'Woche 11:'
  end
  object Label12: TLabel
    Left = 24
    Top = 393
    Width = 52
    Height = 13
    Caption = 'Woche 12:'
  end
  object Label13: TLabel
    Left = 24
    Top = 417
    Width = 52
    Height = 13
    Caption = 'Woche 13:'
  end
  object Label14: TLabel
    Left = 160
    Top = 136
    Width = 52
    Height = 13
    Caption = 'Woche 14:'
  end
  object Label15: TLabel
    Left = 160
    Top = 159
    Width = 52
    Height = 13
    Caption = 'Woche 15:'
  end
  object Label16: TLabel
    Left = 160
    Top = 182
    Width = 52
    Height = 13
    Caption = 'Woche 16:'
  end
  object Label17: TLabel
    Left = 160
    Top = 206
    Width = 52
    Height = 13
    Caption = 'Woche 17:'
  end
  object Label18: TLabel
    Left = 160
    Top = 229
    Width = 52
    Height = 13
    Caption = 'Woche 18:'
  end
  object Label19: TLabel
    Left = 160
    Top = 253
    Width = 52
    Height = 13
    Caption = 'Woche 19:'
  end
  object Label20: TLabel
    Left = 160
    Top = 276
    Width = 52
    Height = 13
    Caption = 'Woche 20:'
  end
  object Label21: TLabel
    Left = 160
    Top = 299
    Width = 52
    Height = 13
    Caption = 'Woche 21:'
  end
  object Label22: TLabel
    Left = 160
    Top = 323
    Width = 52
    Height = 13
    Caption = 'Woche 22:'
  end
  object Label23: TLabel
    Left = 160
    Top = 346
    Width = 52
    Height = 13
    Caption = 'Woche 23:'
  end
  object Label24: TLabel
    Left = 160
    Top = 370
    Width = 52
    Height = 13
    Caption = 'Woche 24:'
  end
  object Label25: TLabel
    Left = 160
    Top = 393
    Width = 52
    Height = 13
    Caption = 'Woche 25:'
  end
  object Label26: TLabel
    Left = 160
    Top = 417
    Width = 52
    Height = 13
    Caption = 'Woche 26:'
  end
  object Label27: TLabel
    Left = 296
    Top = 136
    Width = 52
    Height = 13
    Caption = 'Woche 27:'
  end
  object Label28: TLabel
    Left = 296
    Top = 159
    Width = 52
    Height = 13
    Caption = 'Woche 28:'
  end
  object Label29: TLabel
    Left = 296
    Top = 182
    Width = 52
    Height = 13
    Caption = 'Woche 29:'
  end
  object Label30: TLabel
    Left = 296
    Top = 206
    Width = 52
    Height = 13
    Caption = 'Woche 30:'
  end
  object Label31: TLabel
    Left = 296
    Top = 229
    Width = 52
    Height = 13
    Caption = 'Woche 31:'
  end
  object Label32: TLabel
    Left = 296
    Top = 253
    Width = 52
    Height = 13
    Caption = 'Woche 32:'
  end
  object Label33: TLabel
    Left = 296
    Top = 276
    Width = 52
    Height = 13
    Caption = 'Woche 33:'
  end
  object Label34: TLabel
    Left = 296
    Top = 299
    Width = 52
    Height = 13
    Caption = 'Woche 34:'
  end
  object Label35: TLabel
    Left = 296
    Top = 323
    Width = 52
    Height = 13
    Caption = 'Woche 35:'
  end
  object Label36: TLabel
    Left = 296
    Top = 346
    Width = 52
    Height = 13
    Caption = 'Woche 36:'
  end
  object Label37: TLabel
    Left = 296
    Top = 370
    Width = 52
    Height = 13
    Caption = 'Woche 37:'
  end
  object Label38: TLabel
    Left = 296
    Top = 393
    Width = 52
    Height = 13
    Caption = 'Woche 38:'
  end
  object Label39: TLabel
    Left = 296
    Top = 417
    Width = 52
    Height = 13
    Caption = 'Woche 39:'
  end
  object Label40: TLabel
    Left = 432
    Top = 136
    Width = 52
    Height = 13
    Caption = 'Woche 40:'
  end
  object Label41: TLabel
    Left = 432
    Top = 159
    Width = 52
    Height = 13
    Caption = 'Woche 41:'
  end
  object Label42: TLabel
    Left = 432
    Top = 182
    Width = 52
    Height = 13
    Caption = 'Woche 42:'
  end
  object Label43: TLabel
    Left = 432
    Top = 206
    Width = 52
    Height = 13
    Caption = 'Woche 43:'
  end
  object Label44: TLabel
    Left = 432
    Top = 229
    Width = 52
    Height = 13
    Caption = 'Woche 44:'
  end
  object Label45: TLabel
    Left = 432
    Top = 253
    Width = 52
    Height = 13
    Caption = 'Woche 45:'
  end
  object Label46: TLabel
    Left = 432
    Top = 276
    Width = 52
    Height = 13
    Caption = 'Woche 46:'
  end
  object Label47: TLabel
    Left = 432
    Top = 299
    Width = 52
    Height = 13
    Caption = 'Woche 47:'
  end
  object Label48: TLabel
    Left = 432
    Top = 323
    Width = 52
    Height = 13
    Caption = 'Woche 48:'
  end
  object Label49: TLabel
    Left = 432
    Top = 346
    Width = 52
    Height = 13
    Caption = 'Woche 49:'
  end
  object Label50: TLabel
    Left = 432
    Top = 370
    Width = 52
    Height = 13
    Caption = 'Woche 50:'
  end
  object Label51: TLabel
    Left = 432
    Top = 393
    Width = 52
    Height = 13
    Caption = 'Woche 51:'
  end
  object Label52: TLabel
    Left = 432
    Top = 417
    Width = 52
    Height = 13
    Caption = 'Woche 52:'
  end
  object LabelAbteilung: TLabel
    Left = 24
    Top = 71
    Width = 49
    Height = 13
    Caption = 'Abteilung:'
  end
  object ComboBoxMaschine: TComboBox
    Left = 210
    Top = 91
    Width = 161
    Height = 21
    Style = csDropDownList
    TabOrder = 0
    OnChange = ComboBoxMaschineChange
  end
  object EditWoche1: TEdit
    Left = 88
    Top = 133
    Width = 45
    Height = 21
    TabOrder = 1
  end
  object EditWoche2: TEdit
    Left = 88
    Top = 156
    Width = 45
    Height = 21
    TabOrder = 2
  end
  object EditWoche3: TEdit
    Left = 88
    Top = 179
    Width = 45
    Height = 21
    TabOrder = 3
  end
  object EditWoche4: TEdit
    Left = 88
    Top = 203
    Width = 45
    Height = 21
    TabOrder = 4
  end
  object EditWoche5: TEdit
    Left = 88
    Top = 226
    Width = 45
    Height = 21
    TabOrder = 5
  end
  object EditWoche6: TEdit
    Left = 88
    Top = 250
    Width = 45
    Height = 21
    TabOrder = 6
  end
  object EditWoche7: TEdit
    Left = 88
    Top = 273
    Width = 45
    Height = 21
    TabOrder = 7
  end
  object EditWoche8: TEdit
    Left = 88
    Top = 296
    Width = 45
    Height = 21
    TabOrder = 8
  end
  object EditWoche9: TEdit
    Left = 88
    Top = 320
    Width = 45
    Height = 21
    TabOrder = 9
  end
  object EditWoche10: TEdit
    Left = 88
    Top = 343
    Width = 45
    Height = 21
    TabOrder = 10
  end
  object EditWoche11: TEdit
    Left = 88
    Top = 367
    Width = 45
    Height = 21
    TabOrder = 11
  end
  object EditWoche12: TEdit
    Left = 88
    Top = 390
    Width = 45
    Height = 21
    TabOrder = 12
  end
  object EditWoche13: TEdit
    Left = 88
    Top = 414
    Width = 45
    Height = 21
    TabOrder = 13
  end
  object EditWoche14: TEdit
    Left = 220
    Top = 133
    Width = 45
    Height = 21
    TabOrder = 14
  end
  object EditWoche15: TEdit
    Left = 220
    Top = 156
    Width = 45
    Height = 21
    TabOrder = 15
  end
  object EditWoche16: TEdit
    Left = 220
    Top = 179
    Width = 45
    Height = 21
    TabOrder = 16
  end
  object EditWoche17: TEdit
    Left = 220
    Top = 203
    Width = 45
    Height = 21
    TabOrder = 17
  end
  object EditWoche18: TEdit
    Left = 220
    Top = 226
    Width = 45
    Height = 21
    TabOrder = 18
  end
  object EditWoche19: TEdit
    Left = 220
    Top = 250
    Width = 45
    Height = 21
    TabOrder = 19
  end
  object EditWoche20: TEdit
    Left = 220
    Top = 273
    Width = 45
    Height = 21
    TabOrder = 20
  end
  object EditWoche21: TEdit
    Left = 220
    Top = 296
    Width = 45
    Height = 21
    TabOrder = 21
  end
  object EditWoche22: TEdit
    Left = 220
    Top = 320
    Width = 45
    Height = 21
    TabOrder = 22
  end
  object EditWoche23: TEdit
    Left = 220
    Top = 343
    Width = 45
    Height = 21
    TabOrder = 23
  end
  object EditWoche24: TEdit
    Left = 220
    Top = 367
    Width = 45
    Height = 21
    TabOrder = 24
  end
  object EditWoche25: TEdit
    Left = 220
    Top = 390
    Width = 45
    Height = 21
    TabOrder = 25
  end
  object EditWoche26: TEdit
    Left = 220
    Top = 414
    Width = 45
    Height = 21
    TabOrder = 26
  end
  object EditWoche27: TEdit
    Left = 356
    Top = 133
    Width = 45
    Height = 21
    TabOrder = 27
  end
  object EditWoche28: TEdit
    Left = 356
    Top = 156
    Width = 45
    Height = 21
    TabOrder = 28
  end
  object EditWoche29: TEdit
    Left = 356
    Top = 179
    Width = 45
    Height = 21
    TabOrder = 29
  end
  object EditWoche30: TEdit
    Left = 356
    Top = 203
    Width = 45
    Height = 21
    TabOrder = 30
  end
  object EditWoche31: TEdit
    Left = 356
    Top = 226
    Width = 45
    Height = 21
    TabOrder = 31
  end
  object EditWoche32: TEdit
    Left = 356
    Top = 250
    Width = 45
    Height = 21
    TabOrder = 32
  end
  object EditWoche33: TEdit
    Left = 356
    Top = 273
    Width = 45
    Height = 21
    TabOrder = 33
  end
  object EditWoche34: TEdit
    Left = 356
    Top = 296
    Width = 45
    Height = 21
    TabOrder = 34
  end
  object EditWoche35: TEdit
    Left = 356
    Top = 320
    Width = 45
    Height = 21
    TabOrder = 35
  end
  object EditWoche36: TEdit
    Left = 356
    Top = 343
    Width = 45
    Height = 21
    TabOrder = 36
  end
  object EditWoche37: TEdit
    Left = 356
    Top = 367
    Width = 45
    Height = 21
    TabOrder = 37
  end
  object EditWoche38: TEdit
    Left = 356
    Top = 390
    Width = 45
    Height = 21
    TabOrder = 38
  end
  object EditWoche39: TEdit
    Left = 356
    Top = 414
    Width = 45
    Height = 21
    TabOrder = 39
  end
  object EditWoche40: TEdit
    Left = 492
    Top = 133
    Width = 45
    Height = 21
    TabOrder = 40
  end
  object EditWoche41: TEdit
    Left = 492
    Top = 156
    Width = 45
    Height = 21
    TabOrder = 41
  end
  object EditWoche42: TEdit
    Left = 492
    Top = 179
    Width = 45
    Height = 21
    TabOrder = 42
  end
  object EditWoche43: TEdit
    Left = 492
    Top = 203
    Width = 45
    Height = 21
    TabOrder = 43
  end
  object EditWoche44: TEdit
    Left = 492
    Top = 226
    Width = 45
    Height = 21
    TabOrder = 44
  end
  object EditWoche45: TEdit
    Left = 492
    Top = 250
    Width = 45
    Height = 21
    TabOrder = 45
  end
  object EditWoche46: TEdit
    Left = 492
    Top = 273
    Width = 45
    Height = 21
    TabOrder = 46
  end
  object EditWoche47: TEdit
    Left = 492
    Top = 296
    Width = 45
    Height = 21
    TabOrder = 47
  end
  object EditWoche48: TEdit
    Left = 492
    Top = 320
    Width = 45
    Height = 21
    TabOrder = 48
  end
  object EditWoche49: TEdit
    Left = 492
    Top = 343
    Width = 45
    Height = 21
    TabOrder = 49
  end
  object EditWoche50: TEdit
    Left = 492
    Top = 367
    Width = 45
    Height = 21
    TabOrder = 50
  end
  object EditWoche51: TEdit
    Left = 492
    Top = 390
    Width = 45
    Height = 21
    TabOrder = 51
  end
  object EditWoche52: TEdit
    Left = 492
    Top = 414
    Width = 45
    Height = 21
    TabOrder = 52
  end
  object ButtonOk: TButton
    Left = 176
    Top = 464
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 53
    OnClick = ButtonOkClick
  end
  object ButtonCancel: TButton
    Left = 296
    Top = 464
    Width = 75
    Height = 25
    Caption = 'Abbrechen'
    TabOrder = 54
    OnClick = ButtonCancelClick
  end
  object ComboBoxAbteilung: TComboBox
    Left = 24
    Top = 90
    Width = 161
    Height = 21
    Style = csDropDownList
    TabOrder = 55
    OnChange = ComboBoxAbteilungChange
  end
  object FDPhysMSAccessDriverLink: TFDPhysMSAccessDriverLink
    Left = 104
    Top = 216
  end
  object FDConnection: TFDConnection
    Params.Strings = (
      'DriverID=MSAcc')
    LoginPrompt = False
    Left = 112
    Top = 272
  end
  object FDMSAccessService: TFDMSAccessService
    Database = 'Gloor.accdb'
    Left = 112
    Top = 328
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    ScreenCursor = gcrHourGlass
    Left = 440
    Top = 216
  end
  object FDGUIxErrorDialog: TFDGUIxErrorDialog
    Provider = 'Forms'
    Caption = 'FireDAC Executor Error'
    Left = 440
    Top = 280
  end
  object FDGUIxLoginDialog: TFDGUIxLoginDialog
    Provider = 'Forms'
    Left = 440
    Top = 336
  end
  object FDQueryMaschine: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT *  FROM Maschinen')
    Left = 192
    Top = 208
  end
  object FDQueryGetMaschineId: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT ID FROM Maschinen WHERE Maschine = :Maschine')
    Left = 192
    Top = 264
    ParamData = <
      item
        Name = 'MASCHINE'
        ParamType = ptInput
      end>
  end
  object FDQueryGetKapazitaet: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      
        'SELECT * FROM Kapazitaeten WHERE Maschine = :Maschine ORDER BY W' +
        'oche')
    Left = 192
    Top = 336
    ParamData = <
      item
        Name = 'MASCHINE'
        ParamType = ptInput
      end>
  end
  object FDQueryDeleteKapazitaet: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'DELETE FROM Kapazitaeten WHERE Maschine = :Maschine')
    Left = 192
    Top = 384
    ParamData = <
      item
        Name = 'MASCHINE'
        ParamType = ptInput
      end>
  end
  object FDQueryAddKapazitaet: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'INSERT INTO Kapazitaeten '
      '(Maschine, Woche, Kapazitaet)'
      'VALUES '
      '(:Maschine, :Woche, :Kapazitaet)')
    Left = 192
    Top = 432
    ParamData = <
      item
        Name = 'MASCHINE'
        ParamType = ptInput
      end
      item
        Name = 'WOCHE'
        ParamType = ptInput
      end
      item
        Name = 'KAPAZITAET'
        ParamType = ptInput
      end>
  end
  object FDQueryAbteilung: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT * FROM Abteilungen')
    Left = 176
    Top = 128
  end
  object FDQueryMaschineInAbteilung: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT *  FROM Maschinen WHERE Abteilung = :Abteilung')
    Left = 312
    Top = 208
    ParamData = <
      item
        Name = 'ABTEILUNG'
        ParamType = ptInput
      end>
  end
  object FDQueryGetAbteilungsId: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT ID FROM Abteilungen WHERE Abteilung = :Abteilung')
    Left = 310
    Top = 128
    ParamData = <
      item
        Name = 'ABTEILUNG'
        ParamType = ptInput
      end>
  end
end
