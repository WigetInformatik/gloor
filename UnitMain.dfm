object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = 'Fertigungsplanung - Gloor AG'
  ClientHeight = 861
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  DesignSize = (
    1264
    861)
  PixelsPerInch = 96
  TextHeight = 13
  object LabelFertigungsplanung: TLabel
    Left = 24
    Top = 24
    Width = 220
    Height = 33
    Caption = 'Fertigungsplanung'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object ButtonNeuerAuftrag: TButton
    Left = 24
    Top = 80
    Width = 200
    Height = 57
    Caption = 'Neuer Auftrag'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = ButtonNeuerAuftragClick
  end
  object ButtonAuftragSuchen: TButton
    Left = 430
    Top = 80
    Width = 200
    Height = 57
    Caption = 'Auftrag suchen'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = ButtonAuftragSuchenClick
  end
  object ButtonKapazitaetDefinieren: TButton
    Left = 633
    Top = 80
    Width = 200
    Height = 57
    Caption = 'Kapazit'#228't definieren'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = ButtonKapazitaetDefinierenClick
  end
  object ButtonAuftragDrucken: TButton
    Left = 836
    Top = 80
    Width = 200
    Height = 57
    Caption = 'Auftr'#228'ge drucken'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = ButtonAuftragDruckenClick
  end
  object ComboBoxAuftrag: TComboBox
    Left = 24
    Top = 160
    Width = 161
    Height = 21
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 4
    Text = 'Alle Auftr'#228'ge'
    OnChange = UpdateListe
    Items.Strings = (
      'Alle Auftr'#228'ge'
      'Offene Auftr'#228'ge')
  end
  object ComboBoxAbteilung: TComboBox
    Left = 216
    Top = 160
    Width = 217
    Height = 21
    Style = csDropDownList
    TabOrder = 5
    OnChange = ComboBoxAbteilungChange
  end
  object ButtonKonfiguration: TButton
    Left = 1040
    Top = 80
    Width = 200
    Height = 57
    Caption = 'Konfiguration'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = ButtonKonfigurationClick
  end
  object AdvStringGridAuftrag: TAdvStringGrid
    Left = 24
    Top = 200
    Width = 1225
    Height = 653
    Cursor = crDefault
    Anchors = [akLeft, akTop, akRight, akBottom]
    DrawingStyle = gdsClassic
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    ScrollBars = ssBoth
    TabOrder = 7
    HoverRowCells = [hcNormal, hcSelected]
    OnClickCell = AdvStringGridAuftragClickCell
    OnDblClickCell = AdvStringGridAuftragDblClickCell
    ActiveCellFont.Charset = DEFAULT_CHARSET
    ActiveCellFont.Color = clWindowText
    ActiveCellFont.Height = -11
    ActiveCellFont.Name = 'Tahoma'
    ActiveCellFont.Style = [fsBold]
    ColumnSize.Key = 'SOFTWARE\Wiget Informatik\Gloor Produktionsplanung'
    ColumnSize.Section = 'MainAuftrag'
    ControlLook.FixedGradientHoverFrom = clGray
    ControlLook.FixedGradientHoverTo = clWhite
    ControlLook.FixedGradientDownFrom = clGray
    ControlLook.FixedGradientDownTo = clSilver
    ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
    ControlLook.DropDownHeader.Font.Color = clWindowText
    ControlLook.DropDownHeader.Font.Height = -11
    ControlLook.DropDownHeader.Font.Name = 'Tahoma'
    ControlLook.DropDownHeader.Font.Style = []
    ControlLook.DropDownHeader.Visible = True
    ControlLook.DropDownHeader.Buttons = <>
    ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
    ControlLook.DropDownFooter.Font.Color = clWindowText
    ControlLook.DropDownFooter.Font.Height = -11
    ControlLook.DropDownFooter.Font.Name = 'Tahoma'
    ControlLook.DropDownFooter.Font.Style = []
    ControlLook.DropDownFooter.Visible = True
    ControlLook.DropDownFooter.Buttons = <>
    Filter = <>
    FilterDropDown.Font.Charset = DEFAULT_CHARSET
    FilterDropDown.Font.Color = clWindowText
    FilterDropDown.Font.Height = -11
    FilterDropDown.Font.Name = 'Tahoma'
    FilterDropDown.Font.Style = []
    FilterDropDown.TextChecked = 'Checked'
    FilterDropDown.TextUnChecked = 'Unchecked'
    FilterDropDownClear = '(All)'
    FilterEdit.TypeNames.Strings = (
      'Starts with'
      'Ends with'
      'Contains'
      'Not contains'
      'Equal'
      'Not equal'
      'Larger than'
      'Smaller than'
      'Clear')
    FixedColWidth = 60
    FixedRowHeight = 22
    FixedFont.Charset = DEFAULT_CHARSET
    FixedFont.Color = clWindowText
    FixedFont.Height = -11
    FixedFont.Name = 'Tahoma'
    FixedFont.Style = [fsBold]
    FloatFormat = '%.2f'
    HoverButtons.Buttons = <>
    HoverButtons.Position = hbLeftFromColumnLeft
    HTMLSettings.ImageFolder = 'images'
    HTMLSettings.ImageBaseName = 'img'
    PrintSettings.DateFormat = 'dd/mm/yyyy'
    PrintSettings.Font.Charset = DEFAULT_CHARSET
    PrintSettings.Font.Color = clWindowText
    PrintSettings.Font.Height = -11
    PrintSettings.Font.Name = 'Tahoma'
    PrintSettings.Font.Style = []
    PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
    PrintSettings.FixedFont.Color = clWindowText
    PrintSettings.FixedFont.Height = -11
    PrintSettings.FixedFont.Name = 'Tahoma'
    PrintSettings.FixedFont.Style = []
    PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
    PrintSettings.HeaderFont.Color = clWindowText
    PrintSettings.HeaderFont.Height = -11
    PrintSettings.HeaderFont.Name = 'Tahoma'
    PrintSettings.HeaderFont.Style = []
    PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
    PrintSettings.FooterFont.Color = clWindowText
    PrintSettings.FooterFont.Height = -11
    PrintSettings.FooterFont.Name = 'Tahoma'
    PrintSettings.FooterFont.Style = []
    PrintSettings.PageNumSep = '/'
    SearchFooter.FindNextCaption = 'Find &next'
    SearchFooter.FindPrevCaption = 'Find &previous'
    SearchFooter.Font.Charset = DEFAULT_CHARSET
    SearchFooter.Font.Color = clWindowText
    SearchFooter.Font.Height = -11
    SearchFooter.Font.Name = 'Tahoma'
    SearchFooter.Font.Style = []
    SearchFooter.HighLightCaption = 'Highlight'
    SearchFooter.HintClose = 'Close'
    SearchFooter.HintFindNext = 'Find next occurrence'
    SearchFooter.HintFindPrev = 'Find previous occurrence'
    SearchFooter.HintHighlight = 'Highlight occurrences'
    SearchFooter.MatchCaseCaption = 'Match case'
    SearchFooter.ResultFormat = '(%d of %d)'
    SortSettings.DefaultFormat = ssAutomatic
    Version = '8.3.5.2'
    ColWidths = (
      60
      64
      64
      64
      64)
  end
  object ButtonAuftragBearbeiten: TButton
    Left = 227
    Top = 80
    Width = 200
    Height = 57
    Caption = 'Auftrag bearbeiten'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    Visible = False
    OnClick = ButtonAuftragBearbeitenClick
  end
  object ComboBoxMaschine: TComboBox
    Left = 456
    Top = 160
    Width = 217
    Height = 21
    Style = csDropDownList
    TabOrder = 9
    OnChange = UpdateListe
  end
  object FDPhysMSAccessDriverLink: TFDPhysMSAccessDriverLink
    Left = 472
    Top = 256
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    ScreenCursor = gcrHourGlass
    Left = 952
    Top = 264
  end
  object FDGUIxErrorDialog: TFDGUIxErrorDialog
    Provider = 'Forms'
    Caption = 'FireDAC Executor Error'
    Left = 952
    Top = 328
  end
  object FDGUIxLoginDialog: TFDGUIxLoginDialog
    Provider = 'Forms'
    Left = 952
    Top = 384
  end
  object FDMSAccessService: TFDMSAccessService
    Database = 'Gloor.accdb'
    Left = 472
    Top = 392
  end
  object FDConnection: TFDConnection
    Params.Strings = (
      'DriverID=MSAcc')
    LoginPrompt = False
    Left = 472
    Top = 320
  end
  object FDQueryAbteilungen: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT * FROM Abteilungen')
    Left = 616
    Top = 256
  end
  object FDQueryGetAuftrag: TFDQuery
    Connection = FDConnection
    Left = 624
    Top = 328
  end
  object FDQueryGetPlanung: TFDQuery
    Connection = FDConnection
    Left = 624
    Top = 384
  end
  object FDQueryGetAbteilungsId: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT ID FROM Abteilungen WHERE Abteilung = :Abteilung')
    Left = 736
    Top = 240
    ParamData = <
      item
        Name = 'ABTEILUNG'
        ParamType = ptInput
      end>
  end
  object FDQueryGetMaschineId: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT ID FROM Maschinen WHERE Maschine = :Maschine')
    Left = 736
    Top = 312
    ParamData = <
      item
        Name = 'MASCHINE'
        ParamType = ptInput
      end>
  end
  object FDQueryMaschineInAbteilung: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT *  FROM Maschinen WHERE Abteilung = :Abteilung')
    Left = 632
    Top = 456
    ParamData = <
      item
        Name = 'ABTEILUNG'
        ParamType = ptInput
      end>
  end
end
