unit UnitMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Phys.MSAccDef,
  FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.VCLUI.Error, FireDAC.Stan.Error,
  FireDAC.VCLUI.Login, FireDAC.Stan.Def, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Phys.Intf, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MSAcc, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Phys.ODBCBase, FireDAC.Comp.UI, AdvUtil, Vcl.Grids, AdvObj, BaseGrid,
  AdvGrid;

type
  TFormMain = class(TForm)
    LabelFertigungsplanung: TLabel;
    ButtonNeuerAuftrag: TButton;
    ButtonAuftragSuchen: TButton;
    ButtonKapazitaetDefinieren: TButton;
    ButtonAuftragDrucken: TButton;
    ComboBoxAuftrag: TComboBox;
    ComboBoxAbteilung: TComboBox;
    FDPhysMSAccessDriverLink: TFDPhysMSAccessDriverLink;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    FDGUIxErrorDialog: TFDGUIxErrorDialog;
    FDGUIxLoginDialog: TFDGUIxLoginDialog;
    FDMSAccessService: TFDMSAccessService;
    FDConnection: TFDConnection;
    FDQueryAbteilungen: TFDQuery;
    ButtonKonfiguration: TButton;
    AdvStringGridAuftrag: TAdvStringGrid;
    FDQueryGetAuftrag: TFDQuery;
    FDQueryGetPlanung: TFDQuery;
    FDQueryGetAbteilungsId: TFDQuery;
    FDQueryGetMaschineId: TFDQuery;
    ButtonAuftragBearbeiten: TButton;
    ComboBoxMaschine: TComboBox;
    FDQueryMaschineInAbteilung: TFDQuery;
    procedure FormCreate(Sender: TObject);
    procedure ButtonNeuerAuftragClick(Sender: TObject);
    procedure ButtonAuftragSuchenClick(Sender: TObject);
    procedure ButtonAuftragDruckenClick(Sender: TObject);
    procedure ButtonKapazitaetDefinierenClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure ButtonKonfigurationClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UpdateListe(Sender: TObject);
    procedure AdvStringGridAuftragClickCell(Sender: TObject; ARow,
      ACol: Integer);
    procedure AdvStringGridAuftragDblClickCell(Sender: TObject; ARow,
      ACol: Integer);
    procedure ButtonAuftragBearbeitenClick(Sender: TObject);
    procedure ComboBoxAbteilungChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private-Deklarationen }
    FAbteilungen : TStringList;
    FMaschinenListe : TStringList;
    FAuftragId : Integer;
    FDbName : String;
    FAbteilungReg : String;
    FMaschineReg : String;
    FAuftragReg : String;
    function GetAlleAuftraege : String;
    function GetAlleAuftraegeVon(Abteilung : Integer) : String;
    function GetAlleAuftraegeVonMaschine(Maschine : Integer) : String;
    function GetOffeneAuftraege : String;
    function GetOffeneAuftraegeVon(Abteilung : Integer) : String;
    function GetOffeneAuftraegeVonMaschine(Maschine : Integer) : String;
    function GetAbteilungId (Abteilung : String) : Integer;
    function GetMaschineId (Maschine : String) : Integer;
  public
    { Public-Deklarationen }
    property DbName : String read FDbName;
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

uses Registry, VCL.TMSLogging, TMSLoggingTextOutputHandler,
  UnitAuftrag, UnitAuftragDrucken, UnitAuftragSuchen,
  UnitKapazitaetDefinieren, UnitKonfiguration;

const
  RegKey = 'SOFTWARE\Wiget Informatik\Gloor Produktionsplanung\';

function TFormMain.GetAbteilungId (Abteilung: string) : Integer;
begin
  FDQueryGetAbteilungsId.ParamByName('Abteilung').AsString := Abteilung;
  FDQueryGetAbteilungsId.Open;
  FDQueryGetAbteilungsId.First;
  Result := FDQueryGetAbteilungsId.FieldByName('ID').AsInteger;
  FDQueryGetAbteilungsId.Close;
end;

function TFormMain.GetMaschineId (Maschine: string) : Integer;
begin
  FDQueryGetMaschineId.ParamByName('Maschine').AsString := Maschine;
  FDQueryGetMaschineId.Open;
  FDQueryGetMaschineId.First;
  Result := FDQueryGetMaschineId.FieldByName('ID').AsInteger;
  FDQueryGetMaschineId.Close;
end;

procedure TFormMain.AdvStringGridAuftragClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if ARow > 0 then begin
    FAuftragId := StrToInt(AdvStringGridAuftrag.Cells[0, ARow]);
    AdvStringGridAuftrag.SelectRows(ARow, 1);
    ButtonAuftragBearbeiten.Visible := True;
  end;
end;

procedure TFormMain.AdvStringGridAuftragDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if ARow > 0 then begin
    AdvStringGridAuftrag.SelectRows(ARow, 1);
    FormAuftrag.AuftragId := StrToInt(AdvStringGridAuftrag.Cells[0, ARow]);
    FormAuftrag.ShowModal;
    Sleep(50);
    UpdateListe(Sender);
  end;
end;

procedure TFormMain.ButtonAuftragBearbeitenClick(Sender: TObject);
begin
  if FAuftragId > 0 then begin
    FormAuftrag.AuftragId := FAuftragId;
    FormAuftrag.ShowModal;
    Sleep(50);
    UpdateListe(Sender);
  end
  else
    ButtonAuftragBearbeiten.Visible := False;
end;

procedure TFormMain.ButtonAuftragDruckenClick(Sender: TObject);
begin
  FormAuftragDrucken.ShowModal;
end;

procedure TFormMain.ButtonAuftragSuchenClick(Sender: TObject);
begin
  FormAuftragSuchen.ShowModal;
end;

procedure TFormMain.ButtonKapazitaetDefinierenClick(Sender: TObject);
begin
  FormKapazitaetDefinieren.ShowModal;
end;

procedure TFormMain.ButtonKonfigurationClick(Sender: TObject);
begin
  FormKonfiguration.ShowModal;
end;

procedure TFormMain.ButtonNeuerAuftragClick(Sender: TObject);
begin
  FormAuftrag.AuftragId := -1;
  FormAuftrag.ShowModal;
  UpdateListe(Sender);
end;

procedure TFormMain.ComboBoxAbteilungChange(Sender: TObject);
begin
  // Update Maschinen ComboBox
  FMaschinenListe.Clear;

  FDQueryMaschineInAbteilung.ParamByName('Abteilung').AsInteger := GetAbteilungId (ComboBoxAbteilung.Text);
  FDQueryMaschineInAbteilung.Active := True;
  FDQueryMaschineInAbteilung.First;
  FMaschinenListe.Add('Alle');
  while not FDQueryMaschineInAbteilung.Eof do begin
    FMaschinenListe.Add(FDQueryMaschineInAbteilung.FieldByName('Maschine').AsString);
    FDQueryMaschineInAbteilung.Next;
  end;
  FDQueryMaschineInAbteilung.Close;

  ComboBoxMaschine.Items := FMaschinenListe;
  ComboBoxMaschine.ItemIndex := -1;

  UpdateListe(Sender);
end;

procedure TFormMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Reg : TRegistry;
begin
  // Einstellungen in Registry speichern
  Reg := TRegistry.Create(KEY_READ);
  try
    Reg.Access := KEY_WRITE;
    Reg.OpenKey(RegKey, True);
    Reg.WriteString('Auftrag', ComboBoxAuftrag.Text);
    Reg.WriteString('Abteilung', ComboBoxAbteilung.Text);
    Reg.WriteString('Maschine', ComboBoxMaschine.Text);
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
end;

procedure TFormMain.FormCreate(Sender: TObject);
var
  Reg : TRegistry;
  DateTime : String;
  Logfile : String;
begin
  FormMain.Position := poScreenCenter;
  Reg := TRegistry.Create;
  try
    if Reg.OpenKey(RegKey, False) then begin
      FAuftragReg := Reg.ReadString('Auftrag');
      FAbteilungReg := Reg.ReadString('Abteilung');
      FMaschineReg := Reg.ReadString('Maschine');
      // ComboBoxAuftrag.ItemIndex := Reg.ReadInteger('Auftrag');
      // ComboBoxAbteilung.ItemIndex := Reg.ReadInteger('Abteilung');
      FDbName := Reg.ReadString('DbName');
      if not Reg.ValueExists('Neu') then
        Reg.WriteInteger('Neu', clGreen);
      if not Reg.ValueExists('Express') then
        Reg.WriteInteger('Express', clYellow);
      if not Reg.ValueExists('Reklamation') then
        Reg.WriteInteger('Reklamation', clRed);
    end;
    if FormAuftrag.Logging then begin
      DateTimeToString(DateTime, 'yyyymmdd hhmm', Now);
      Logfile := ExtractFilePath (FDbName) + '\Log\' + DateTime + ' - ' + GetEnvironmentVariable('COMPUTERNAME') + '.txt';
      if not DirectoryExists (ExtractFilePath (Logfile)) then
        CreateDir (ExtractFilePath (Logfile));
      TMSLogger.RegisterOutputHandlerClass(TTMSLoggerTextOutputHandler, [Logfile]);
    end;
  finally
    Reg.CloseKey;
    Reg.Free;
  end;

  if not System.SysUtils.FileExists(DbName) then begin
    try
      FormKonfiguration := TFormKonfiguration.Create(nil);
      FormKonfiguration.ShowModal;
      FDConnection.Params.Add('Database=' + FormKonfiguration.DbName);
      FDConnection.Open;
    finally
      FormKonfiguration.FreeOnRelease;
    end;
  end;

  FDConnection.Params.Add('DriverID=MSAcc');
  FDConnection.Params.Add('Database=' + DbName);
  FDConnection.Open;

  FAbteilungen := TStringList.Create;
  FDQueryAbteilungen.Active := True;
  FDQueryAbteilungen.First;
  while not FDQueryAbteilungen.EOF do begin
    FAbteilungen.Add(FDQueryAbteilungen.FieldByName('Abteilung').AsString);
    FDQueryAbteilungen.Next;
  end;
  FDQueryAbteilungen.Active := False;

  ComboBoxAbteilung.Items := FAbteilungen;
  ComboBoxAbteilung.ItemIndex := ComboBoxAbteilung.Items.IndexOf(FAbteilungReg);

  // Update Maschinen ComboBox
  FMaschinenListe := TStringList.Create;
  FMaschinenListe.Clear;

  FDQueryMaschineInAbteilung.ParamByName('Abteilung').AsInteger := GetAbteilungId (FAbteilungReg);
  FDQueryMaschineInAbteilung.Active := True;
  FDQueryMaschineInAbteilung.First;
  FMaschinenListe.Add('Alle');
  while not FDQueryMaschineInAbteilung.Eof do begin
    FMaschinenListe.Add(FDQueryMaschineInAbteilung.FieldByName('Maschine').AsString);
    FDQueryMaschineInAbteilung.Next;
  end;
  FDQueryMaschineInAbteilung.Close;

  ComboBoxMaschine.Items := FMaschinenListe;

  ComboBoxMaschine.ItemIndex := ComboBoxMaschine.Items.IndexOf(FMaschineReg);

  ComboBoxAuftrag.ItemIndex := ComboBoxAuftrag.Items.IndexOf(FAuftragReg);
end;

procedure TFormMain.FormDestroy(Sender: TObject);
begin
  FDConnection.Close;
end;

procedure TFormMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = Vk_F5 then
    UpdateListe(Sender);
end;

procedure TFormMain.FormShow(Sender: TObject);
begin
  UpdateListe(Sender);
end;

function TFormMain.GetAlleAuftraege : String;
begin
  Result := 'SELECT Auftrag.ID, AB_Nr, Artikel_Nr, Durchmesser, Zaehne, Stueck, ' +
      'Kunden.Kunde, Auftragsart.Auftragsart ' +
      'FROM (Auftrag LEFT JOIN Kunden ON Auftrag.Kunde = Kunden.ID) ' +
      'LEFT JOIN Auftragsart ON Auftrag.Auftragsart = Auftragsart.ID';
  {
  Result := 'SELECT Planung.Kommentar, Vorgabe, Woche, Erledigt, Datum, ' +
      'Abteilungen.Abteilung , Maschinen.Maschine, ' +
      'Auftrag.ID, AB_Nr, Artikel_Nr, Durchmesser, Zaehne, Stueck, ' +
      'Kunden.Kunde, Auftragsart.Auftragsart ' +
      'FROM ((((Planung LEFT JOIN Abteilungen ON Planung.Abteilung = Abteilungen.ID) ' +
      'LEFT JOIN Maschinen ON Planung.Maschine = Maschinen.ID) ' +
      'RIGHT JOIN Auftrag ON Planung.Auftrag = Auftrag.ID) ' +
      'LEFT JOIN Kunden ON Auftrag.Kunde = Kunden.ID) ' +
      'LEFT JOIN Auftragsart ON Auftrag.Auftragsart = Auftragsart.ID ' +
      'ORDER BY Woche, Auftrag.ID';
  }
end;

function TFormMain.GetAlleAuftraegeVon(Abteilung: Integer) : String;
begin
  Result := 'SELECT Planung.Kommentar, Vorgabe, Woche, Erledigt, Datum, ' +
      'Abteilungen.Abteilung , Maschinen.Maschine, ' +
      'Auftrag.ID, AB_Nr, Artikel_Nr, Durchmesser, Zaehne, Stueck, ' +
      'Kunden.Kunde, Auftragsart.Auftragsart ' +
      'FROM ((((Planung LEFT JOIN Abteilungen ON Planung.Abteilung = Abteilungen.ID) ' +
      'LEFT JOIN Maschinen ON Planung.Maschine = Maschinen.ID) ' +
      'RIGHT JOIN Auftrag ON Planung.Auftrag = Auftrag.ID) ' +
      'LEFT JOIN Kunden ON Auftrag.Kunde = Kunden.ID) ' +
      'LEFT JOIN Auftragsart ON Auftrag.Auftragsart = Auftragsart.ID ' +
      'WHERE Planung.Abteilung = ' + IntToStr (Abteilung) + ' ' +
      'ORDER BY Woche, Auftrag.ID';
end;

function TFormMain.GetAlleAuftraegeVonMaschine(Maschine: Integer): String;
begin
  Result := 'SELECT Planung.Kommentar, Vorgabe, Woche, Erledigt, Datum, ' +
      'Abteilungen.Abteilung , Maschinen.Maschine, ' +
      'Auftrag.ID, AB_Nr, Artikel_Nr, Durchmesser, Zaehne, Stueck, ' +
      'Kunden.Kunde, Auftragsart.Auftragsart ' +
      'FROM ((((Planung LEFT JOIN Abteilungen ON Planung.Abteilung = Abteilungen.ID) ' +
      'LEFT JOIN Maschinen ON Planung.Maschine = Maschinen.ID) ' +
      'RIGHT JOIN Auftrag ON Planung.Auftrag = Auftrag.ID) ' +
      'LEFT JOIN Kunden ON Auftrag.Kunde = Kunden.ID) ' +
      'LEFT JOIN Auftragsart ON Auftrag.Auftragsart = Auftragsart.ID ' +
      'WHERE Planung.Maschine = ' + IntToStr (Maschine) + ' ' +
      'ORDER BY Woche, Auftrag.ID';
end;

function TFormMain.GetOffeneAuftraege : String;
begin
  Result := 'SELECT Planung.Kommentar, Vorgabe, Woche, Erledigt, Datum, ' +
      'Abteilungen.Abteilung , Maschinen.Maschine, ' +
      'Auftrag.ID, AB_Nr, Artikel_Nr, Durchmesser, Zaehne, Stueck, ' +
      'Kunden.Kunde, Auftragsart.Auftragsart ' +
      'FROM ((((Planung LEFT JOIN Abteilungen ON Planung.Abteilung = Abteilungen.ID) ' +
      'LEFT JOIN Maschinen ON Planung.Maschine = Maschinen.ID) ' +
      'RIGHT JOIN Auftrag ON Planung.Auftrag = Auftrag.ID) ' +
      'LEFT JOIN Kunden ON Auftrag.Kunde = Kunden.ID) ' +
      'LEFT JOIN Auftragsart ON Auftrag.Auftragsart = Auftragsart.ID ' +
      'WHERE Planung.Erledigt = FALSE ' +
      'ORDER BY Woche, Auftrag.ID';
end;

function TFormMain.GetOffeneAuftraegeVon(Abteilung: Integer) : String;
begin
  Result := 'SELECT Planung.Kommentar, Vorgabe, Woche, Erledigt, Datum, ' +
      'Abteilungen.Abteilung , Maschinen.Maschine, ' +
      'Auftrag.ID, AB_Nr, Artikel_Nr, Durchmesser, Zaehne, Stueck, ' +
      'Kunden.Kunde, Auftragsart.Auftragsart ' +
      'FROM ((((Planung LEFT JOIN Abteilungen ON Planung.Abteilung = Abteilungen.ID) ' +
      'LEFT JOIN Maschinen ON Planung.Maschine = Maschinen.ID) ' +
      'RIGHT JOIN Auftrag ON Planung.Auftrag = Auftrag.ID) ' +
      'LEFT JOIN Kunden ON Auftrag.Kunde = Kunden.ID) ' +
      'LEFT JOIN Auftragsart ON Auftrag.Auftragsart = Auftragsart.ID ' +
      'WHERE Planung.Erledigt = FALSE AND Planung.Abteilung = ' + IntToStr (Abteilung) + ' ' +
      'ORDER BY Woche, Auftrag.ID';
end;

function TFormMain.GetOffeneAuftraegeVonMaschine(Maschine: Integer): String;
begin
  Result := 'SELECT Planung.Kommentar, Vorgabe, Woche, Erledigt, Datum, ' +
      'Abteilungen.Abteilung , Maschinen.Maschine, ' +
      'Auftrag.ID, AB_Nr, Artikel_Nr, Durchmesser, Zaehne, Stueck, ' +
      'Kunden.Kunde, Auftragsart.Auftragsart ' +
      'FROM ((((Planung LEFT JOIN Abteilungen ON Planung.Abteilung = Abteilungen.ID) ' +
      'LEFT JOIN Maschinen ON Planung.Maschine = Maschinen.ID) ' +
      'RIGHT JOIN Auftrag ON Planung.Auftrag = Auftrag.ID) ' +
      'LEFT JOIN Kunden ON Auftrag.Kunde = Kunden.ID) ' +
      'LEFT JOIN Auftragsart ON Auftrag.Auftragsart = Auftragsart.ID ' +
      'WHERE Planung.Erledigt = FALSE AND Planung.Maschine = ' + IntToStr (Maschine) + ' ' +
      'ORDER BY Woche, Auftrag.ID';
end;

procedure TFormMain.UpdateListe(Sender: TObject);
var
  SqlString : String;
  row : Integer;
  MitDetails : Boolean;
begin
  MitDetails := True;

  for row := 1 to AdvStringGridAuftrag.RowCount - 1 do begin
    AdvStringGridAuftrag.ClearRows(1, 1);
    AdvStringGridAuftrag.RemoveRows(1, 1);
  end;
  AdvStringGridAuftrag.Clear;

  ButtonAuftragBearbeiten.Visible := False;
  FAuftragId := -1;

  if ComboBoxAuftrag.ItemIndex = 0 then begin
    // Alle Auftr�ge
    if ComboBoxAbteilung.ItemIndex <= 0 then begin
      // Alle Auftr�ge von allen Abteilungen
      SqlString := GetAlleAuftraege;
      MitDetails := False;
    end
    else
      // Alle Auftr�ge einer Abteilung
      if ComboBoxMaschine.ItemIndex <= 0 then
        SqlString := GetAlleAuftraegeVon(GetAbteilungId(ComboBoxAbteilung.Text))
      else
        SqlString := GetAlleAuftraegeVonMaschine(GetMaschineId(ComboBoxMaschine.Text));
  end
  else begin
    // Offene Auftr�ge
    if ComboBoxAbteilung.ItemIndex <= 0 then
      // Offene Auftr�ge aller Abteilungen
      SqlString := GetOffeneAuftraege
    else
      // Offene Auftr�ge einer Abteilung
      if ComboBoxMaschine.ItemIndex <= 0 then
        // Offene Auftr�ge f�r alle Maschinen
        SqlString := GetOffeneAuftraegeVon(GetAbteilungId(ComboBoxAbteilung.Text))
      else
        // Offene Auftr�ge einer Maschine
        SqlString := GetOffeneAuftraegeVonMaschine(GetMaschineId(ComboBoxMaschine.Text));
  end;

  FDQueryGetAuftrag.SQL.Clear;
  FDQueryGetAuftrag.SQL.Add(SqlString);
  FDQueryGetAuftrag.Open;

  AdvStringGridAuftrag.Cells[0, 0] := 'ID';
  AdvStringGridAuftrag.Cells[1, 0] := 'Kunde';
  AdvStringGridAuftrag.Cells[2, 0] := 'AB-Nr';
  AdvStringGridAuftrag.Cells[3, 0] := 'Artikel-Nr';
  AdvStringGridAuftrag.Cells[4, 0] := 'Durchmesser';
  AdvStringGridAuftrag.Cells[5, 0] := 'Z�hne';
  AdvStringGridAuftrag.Cells[6, 0] := 'St�ck';
  AdvStringGridAuftrag.Cells[7, 0] := 'Auftragsart';
  if MitDetails then begin
    AdvStringGridAuftrag.Cells[8, 0] := 'Abteilung';
    AdvStringGridAuftrag.Cells[9, 0] := 'Maschine';
    AdvStringGridAuftrag.Cells[10, 0] := 'Kommentar';
    AdvStringGridAuftrag.Cells[11, 0] := 'Vorgabe';
    AdvStringGridAuftrag.Cells[12, 0] := 'Woche';
    AdvStringGridAuftrag.ColCount := 13;
  end
  else
    AdvStringGridAuftrag.ColCount := 8;

  FDQueryGetAuftrag.First;
  row := 1;
  try
    AdvStringGridAuftrag.BeginUpdate;
    while not FDQueryGetAuftrag.Eof do begin
      AdvStringGridAuftrag.AddRow;
      AdvStringGridAuftrag.Cells[0, row] := FDQueryGetAuftrag.FieldByName('ID').AsString;
      AdvStringGridAuftrag.Cells[1, row] := FDQueryGetAuftrag.FieldByName('Kunde').AsString;
      AdvStringGridAuftrag.Cells[2, row] := FDQueryGetAuftrag.FieldByName('AB_Nr').AsString;
      AdvStringGridAuftrag.Cells[3, row] := FDQueryGetAuftrag.FieldByName('Artikel_Nr').AsString;
      AdvStringGridAuftrag.Cells[4, row] := FDQueryGetAuftrag.FieldByName('Durchmesser').AsString;
      AdvStringGridAuftrag.Cells[5, row] := FDQueryGetAuftrag.FieldByName('Zaehne').AsString;
      AdvStringGridAuftrag.Cells[6, row] := FDQueryGetAuftrag.FieldByName('Stueck').AsString;
      AdvStringGridAuftrag.Cells[7, row] := FDQueryGetAuftrag.FieldByName('Auftragsart').AsString;
      if MitDetails then begin
        AdvStringGridAuftrag.Cells[8, row] := FDQueryGetAuftrag.FieldByName('Abteilung').AsString;
        AdvStringGridAuftrag.Cells[9, row] := FDQueryGetAuftrag.FieldByName('Maschine').AsString;
        AdvStringGridAuftrag.Cells[10, row] := FDQueryGetAuftrag.FieldByName('Kommentar').AsString;
        if FDQueryGetAuftrag.FieldByName('Vorgabe').AsInteger > 0 then
          AdvStringGridAuftrag.Cells[11, row] := FDQueryGetAuftrag.FieldByName('Vorgabe').AsString;
        if FDQueryGetAuftrag.FieldByName('Woche').AsInteger > 0 then
          AdvStringGridAuftrag.Cells[12, row] := FDQueryGetAuftrag.FieldByName('Woche').AsString;
      end;
      if FDQueryGetAuftrag.FieldByName('Auftragsart').AsString = 'Neues Werkzeug' then
        AdvStringGridAuftrag.RowColor[row] := FormKonfiguration.ColorNeu;
      if FDQueryGetAuftrag.FieldByName('Auftragsart').AsString = 'Express' then
        AdvStringGridAuftrag.RowColor[row] := FormKonfiguration.ColorExpress;
      if FDQueryGetAuftrag.FieldByName('Auftragsart').AsString = 'Reklamation' then
        AdvStringGridAuftrag.RowColor[row] := FormKonfiguration.ColorReklamation;
      FDQueryGetAuftrag.Next;
      row := row + 1;
    end;
  finally
    AdvStringGridAuftrag.EndUpdate;
  end;
  FDQueryGetAuftrag.Close;
  AdvStringGridAuftrag.AutoSizeColumns(true, 4);
end;

end.
