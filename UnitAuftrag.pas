unit UnitAuftrag;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls,
  FireDAC.Phys.MSAccDef, FireDAC.Stan.Def, FireDAC.UI.Intf, FireDAC.Stan.Pool,
  FireDAC.Phys, FireDAC.Phys.MSAcc, FireDAC.VCLUI.Wait, FireDAC.VCLUI.Error,
  FireDAC.VCLUI.Login, FireDAC.Comp.UI, FireDAC.Phys.ODBCBase, Vcl.ComCtrls,
  AdvUtil, Vcl.Grids, AdvObj, BaseGrid, AdvGrid, Vcl.Buttons;

type
  TFormAuftrag = class(TForm)
    LabelAuftrag: TLabel;
    LabelKunde: TLabel;
    ComboBoxKunde: TComboBox;
    LabelAbNr: TLabel;
    EditAbNr: TEdit;
    LabelArtikelNr: TLabel;
    EditArtikelNr: TEdit;
    LabelDurchmesser: TLabel;
    EditDurchmesser: TEdit;
    LabelZaehne: TLabel;
    EditZaehne: TEdit;
    LabelStueck: TLabel;
    EditStueck: TEdit;
    LabelAuftragsart: TLabel;
    ComboBoxAuftragsart: TComboBox;
    FDQueryKunde: TFDQuery;
    FDPhysMSAccessDriverLink: TFDPhysMSAccessDriverLink;
    FDMSAccessService: TFDMSAccessService;
    FDConnection: TFDConnection;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    FDGUIxErrorDialog: TFDGUIxErrorDialog;
    FDGUIxLoginDialog: TFDGUIxLoginDialog;
    LabelAbteilung: TLabel;
    ComboBoxAbteilung1: TComboBox;
    ComboBoxAbteilung2: TComboBox;
    ComboBoxAbteilung3: TComboBox;
    ComboBoxAbteilung4: TComboBox;
    ComboBoxAbteilung5: TComboBox;
    ComboBoxAbteilung6: TComboBox;
    ComboBoxAbteilung7: TComboBox;
    ComboBoxAbteilung8: TComboBox;
    ComboBoxAbteilung9: TComboBox;
    ComboBoxAbteilung10: TComboBox;
    ComboBoxAbteilung11: TComboBox;
    ComboBoxAbteilung12: TComboBox;
    ComboBoxAbteilung13: TComboBox;
    ComboBoxAbteilung14: TComboBox;
    ComboBoxAbteilung15: TComboBox;
    FDQueryAbteilung: TFDQuery;
    LabelMaschine: TLabel;
    ComboBoxMaschine1: TComboBox;
    ComboBoxMaschine2: TComboBox;
    ComboBoxMaschine3: TComboBox;
    ComboBoxMaschine4: TComboBox;
    ComboBoxMaschine5: TComboBox;
    ComboBoxMaschine6: TComboBox;
    ComboBoxMaschine7: TComboBox;
    ComboBoxMaschine8: TComboBox;
    ComboBoxMaschine9: TComboBox;
    ComboBoxMaschine10: TComboBox;
    ComboBoxMaschine11: TComboBox;
    ComboBoxMaschine12: TComboBox;
    ComboBoxMaschine13: TComboBox;
    ComboBoxMaschine14: TComboBox;
    ComboBoxMaschine15: TComboBox;
    FDQueryMaschine: TFDQuery;
    LabelKommentar: TLabel;
    EditKommentar1: TEdit;
    EditKommentar2: TEdit;
    EditKommentar3: TEdit;
    EditKommentar4: TEdit;
    EditKommentar5: TEdit;
    EditKommentar6: TEdit;
    EditKommentar7: TEdit;
    EditKommentar8: TEdit;
    EditKommentar9: TEdit;
    EditKommentar10: TEdit;
    EditKommentar11: TEdit;
    EditKommentar12: TEdit;
    EditKommentar13: TEdit;
    EditKommentar14: TEdit;
    EditKommentar15: TEdit;
    LabelVorgabe: TLabel;
    EditVorgabe1: TEdit;
    EditVorgabe2: TEdit;
    EditVorgabe3: TEdit;
    EditVorgabe4: TEdit;
    EditVorgabe5: TEdit;
    EditVorgabe6: TEdit;
    EditVorgabe7: TEdit;
    EditVorgabe8: TEdit;
    EditVorgabe9: TEdit;
    EditVorgabe10: TEdit;
    EditVorgabe11: TEdit;
    EditVorgabe12: TEdit;
    EditVorgabe13: TEdit;
    EditVorgabe14: TEdit;
    EditVorgabe15: TEdit;
    LabelWoche: TLabel;
    EditWoche1: TEdit;
    EditWoche2: TEdit;
    EditWoche3: TEdit;
    EditWoche4: TEdit;
    EditWoche5: TEdit;
    EditWoche6: TEdit;
    EditWoche7: TEdit;
    EditWoche8: TEdit;
    EditWoche9: TEdit;
    EditWoche10: TEdit;
    EditWoche11: TEdit;
    EditWoche12: TEdit;
    EditWoche13: TEdit;
    EditWoche14: TEdit;
    EditWoche15: TEdit;
    CheckBoxErledigt1: TCheckBox;
    LabelErledigt: TLabel;
    CheckBoxErledigt2: TCheckBox;
    CheckBoxErledigt3: TCheckBox;
    CheckBoxErledigt4: TCheckBox;
    CheckBoxErledigt5: TCheckBox;
    CheckBoxErledigt6: TCheckBox;
    CheckBoxErledigt7: TCheckBox;
    CheckBoxErledigt8: TCheckBox;
    CheckBoxErledigt9: TCheckBox;
    CheckBoxErledigt10: TCheckBox;
    CheckBoxErledigt11: TCheckBox;
    CheckBoxErledigt12: TCheckBox;
    CheckBoxErledigt13: TCheckBox;
    CheckBoxErledigt14: TCheckBox;
    CheckBoxErledigt15: TCheckBox;
    FDQueryAuftrag: TFDQuery;
    FDQueryAuftragsart: TFDQuery;
    ButtonOk: TButton;
    ButtonCancel: TButton;
    FDQueryAddKunde: TFDQuery;
    FDQueryGetKundeId: TFDQuery;
    FDQueryAddAuftrag: TFDQuery;
    FDQueryGetAuftragsartId: TFDQuery;
    FDQueryGetAbteilungsId: TFDQuery;
    FDQueryGetMaschineId: TFDQuery;
    FDQueryAddPlanung: TFDQuery;
    FDQueryGetAuftragId: TFDQuery;
    FDQueryGetKunde: TFDQuery;
    FDQueryGetAuftragsart: TFDQuery;
    FDQueryGetAbteilung: TFDQuery;
    FDQueryGetMaschine: TFDQuery;
    FDQueryPlanung: TFDQuery;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    Edit10: TEdit;
    Edit11: TEdit;
    Edit12: TEdit;
    Edit13: TEdit;
    Edit14: TEdit;
    Edit15: TEdit;
    FDQueryUpdateAuftrag: TFDQuery;
    FDQueryUpdatePlanung: TFDQuery;
    FDQueryDeletePlanung: TFDQuery;
    AdvStringGridPlanung: TAdvStringGrid;
    FDQueryGetOffeneAuftraege: TFDQuery;
    FDQueryGetKapazitaet: TFDQuery;
    SpeedButtonDelete1: TSpeedButton;
    SpeedButtonDelete2: TSpeedButton;
    SpeedButtonDelete3: TSpeedButton;
    SpeedButtonDelete4: TSpeedButton;
    SpeedButtonDelete5: TSpeedButton;
    SpeedButtonDelete6: TSpeedButton;
    SpeedButtonDelete7: TSpeedButton;
    SpeedButtonDelete8: TSpeedButton;
    SpeedButtonDelete9: TSpeedButton;
    SpeedButtonDelete10: TSpeedButton;
    SpeedButtonDelete11: TSpeedButton;
    SpeedButtonDelete12: TSpeedButton;
    SpeedButtonDelete13: TSpeedButton;
    SpeedButtonDelete14: TSpeedButton;
    SpeedButtonDelete15: TSpeedButton;
    FDQueryMaschineInAbteilung: TFDQuery;
    ButtonDelete: TButton;
    FDQueryDeleteAuftrag: TFDQuery;
    EditDatum1: TEdit;
    EditDatum2: TEdit;
    EditDatum3: TEdit;
    EditDatum4: TEdit;
    EditDatum5: TEdit;
    EditDatum6: TEdit;
    EditDatum7: TEdit;
    EditDatum8: TEdit;
    EditDatum9: TEdit;
    EditDatum10: TEdit;
    EditDatum11: TEdit;
    EditDatum12: TEdit;
    EditDatum13: TEdit;
    EditDatum14: TEdit;
    EditDatum15: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButtonCancelClick(Sender: TObject);
    procedure ButtonOkClick(Sender: TObject);
    procedure ComboBoxKundeExit(Sender: TObject);
    procedure AbteilungChanged(Sender: TObject);
    procedure MaschineChanged(Sender: TObject);
    procedure VorgabeChanged(Sender: TObject);
    procedure WocheChanged(Sender: TObject);
    procedure KommentarClick(Sender: TObject);
    procedure VorgabeClick(Sender: TObject);
    procedure WocheClick(Sender: TObject);
    procedure ComboBoxAuftragsartChange(Sender: TObject);
    procedure AdvStringGridPlanungGroupCalc(Sender: TObject; ACol, FromRow,
        ToRow: Integer; var Res: Double);
    procedure SpeedButtonDeleteClick(Sender: TObject);
    procedure ButtonDeleteClick(Sender: TObject);
    procedure CheckBoxErledigtClick(Sender: TObject);
  private
    { Private-Deklarationen }
    FKundenListe : TStringList;
    FAbteilungsListe : TStringList;
    FMaschinenListe : TStringList;
    FAuftragsart : TStringList;
    FAuftragId : Integer;
    FButtonDelete : array [1..15] of TSpeedButton;
    FIdList : array [1..15] of TEdit;
    FAbteilungList : array [1..15] of TComboBox;
    FMaschineList : array [1..15] of TComboBox;
    FKommentarList : array [1..15] of TEdit;
    FVorgabeList : array [1..15] of TEdit;
    FWocheList : array [1..15] of TEdit;
    FErledigtList : array [1..15] of TCheckBox;
    FDatumList : array [1..15] of TEdit;
    FUpdateEnabled : Boolean;
    FMaschineId : Integer;
    procedure AddAuftrag;
    procedure UpdateAuftrag;
    procedure UpdateListe (Zeile : Integer);
    function GetKundeId (Kunde : String) : Integer;
    function GetKunde (Id : Integer) : String;
    function GetAuftragId : Integer;
    function GetAuftragsartId (Auftragsart : String) : Integer;
    function GetAuftragsart (Id : Integer) : String;
    function GetAbteilungId (Abteilung : String) : Integer;
    function GetAbteilung (Id : Integer) : String;
    function GetMaschineId (Maschine : String) : Integer;
    function GetMaschine (Id : Integer) : String;
    procedure LoeschePlanung (Zeile : Integer);
  public
    { Public-Deklarationen }
    property AuftragId : Integer read FAuftragId write FAuftragId;
    const Logging : Boolean = False;
  end;

var
  FormAuftrag: TFormAuftrag;

implementation

uses DateUtils, VCL.TMSLogging, UnitKonfiguration, UnitMain;

{$R *.dfm}

function TFormAuftrag.GetKundeId (Kunde: string) : Integer;
begin
  FDQueryGetKundeId.ParamByName('Kunde').AsString := Kunde;
  FDQueryGetKundeId.Open;
  FDQueryGetKundeId.First;
  Result := FDQueryGetKundeId.FieldByName('ID').AsInteger;
  FDQueryGetKundeId.Close;
end;

function TFormAuftrag.GetKunde (Id: Integer) : String;
begin
  FDQueryGetKunde.ParamByName('Id').AsInteger := Id;
  FDQueryGetKunde.Open;
  FDQueryGetKunde.First;
  Result := FDQueryGetKunde.FieldByName('Kunde').AsString;
  FDQueryGetKunde.Close;
end;

function TFormAuftrag.GetAuftragId : Integer;
begin
  FDQueryGetAuftragId.ParamByName('Kunde').AsInteger := GetKundeId (ComboBoxKunde.Text);
  FDQueryGetAuftragId.ParamByName('AB_Nr').AsString := EditAbNr.Text;
  FDQueryGetAuftragId.ParamByName('Artikel_Nr').AsString := EditArtikelNr.Text;
  FDQueryGetAuftragId.ParamByName('Durchmesser').AsString := EditDurchmesser.Text;
  FDQueryGetAuftragId.ParamByName('Zaehne').AsString := EditZaehne.Text;
  FDQueryGetAuftragId.ParamByName('Stueck').AsString := EditStueck.Text;
  FDQueryGetAuftragId.ParamByName('Auftragsart').AsInteger := GetAuftragsartId (ComboBoxAuftragsart.Text);
  FDQueryGetAuftragId.Open;
  FDQueryGetAuftragId.Last;
  Result := FDQueryGetAuftragId.FieldByName('ID').AsInteger;
  FDQueryGetAuftragId.Close;
end;

function TFormAuftrag.GetAuftragsartId (Auftragsart: string) : Integer;
begin
  FDQueryGetAuftragsartId.ParamByName('Auftragsart').AsString := Auftragsart;
  FDQueryGetAuftragsartId.Open;
  FDQueryGetAuftragsartId.First;
  Result := FDQueryGetAuftragsartId.FieldByName('ID').AsInteger;
  FDQueryGetAuftragsartId.Close;
end;

function TFormAuftrag.GetAuftragsart (Id: Integer) : String;
begin
  FDQueryGetAuftragsart.ParamByName('Id').AsInteger := Id;
  FDQueryGetAuftragsart.Open;
  FDQueryGetAuftragsart.First;
  Result := FDQueryGetAuftragsart.FieldByName('Auftragsart').AsString;
  FDQueryGetAuftragsart.Close;
end;

function TFormAuftrag.GetAbteilungId (Abteilung: string) : Integer;
begin
  try
    FDQueryGetAbteilungsId.ParamByName('Abteilung').AsString := Abteilung;
    FDQueryGetAbteilungsId.Open;
    FDQueryGetAbteilungsId.First;
    Result := FDQueryGetAbteilungsId.FieldByName('ID').AsInteger;
  finally
    FDQueryGetAbteilungsId.Close;
  end;
end;

function TFormAuftrag.GetAbteilung (Id: Integer) : String;
begin
  FDQueryGetAbteilung.ParamByName('Id').AsInteger := Id;
  FDQueryGetAbteilung.Open;
  FDQueryGetAbteilung.First;
  Result := FDQueryGetAbteilung.FieldByName('Abteilung').AsString;
  FDQueryGetAbteilung.Close;
end;

function TFormAuftrag.GetMaschineId (Maschine: string) : Integer;
begin
  FDQueryGetMaschineId.ParamByName('Maschine').AsString := Maschine;
  FDQueryGetMaschineId.Open;
  FDQueryGetMaschineId.First;
  Result := FDQueryGetMaschineId.FieldByName('ID').AsInteger;
  FDQueryGetMaschineId.Close;
end;

function TFormAuftrag.GetMaschine (Id: Integer) : String;
begin
  FDQueryGetMaschine.ParamByName('Id').AsInteger := Id;
  FDQueryGetMaschine.Open;
  FDQueryGetMaschine.First;
  Result := FDQueryGetMaschine.FieldByName('Maschine').AsString;
  FDQueryGetMaschine.Close;
end;

procedure TFormAuftrag.AddAuftrag;
var
  i: Integer;
  TempDate : TDateTime;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('AddAuftrag');
    TMSLogger.Debug('AddAuftrag' +
      ' Kunde=' + ComboBoxKunde.Text + ' ('+ IntToStr(GetKundeId(ComboBoxKunde.Text)) + ')' +
      ' AB-Nr=' + EditAbNr.Text +
      ' Artikel-Nr=' + EditArtikelNr.Text +
      ' Durchmesser=' + EditDurchmesser.Text +
      ' Z�hne=' + EditZaehne.Text +
      ' St�ck=' + EditStueck.Text +
      ' Auftragsart=' + ComboBoxAuftragsart.Text + ' (' + IntToStr(GetAuftragsartId(ComboBoxAuftragsart.Text)) + ')');
  end;
  FDQueryAddAuftrag.ParamByName('Kunde').AsInteger := GetKundeId (ComboBoxKunde.Text);
  FDQueryAddAuftrag.ParamByName('AB_Nr').AsString := EditAbNr.Text;
  FDQueryAddAuftrag.ParamByName('Artikel_Nr').AsString := EditArtikelNr.Text;
  FDQueryAddAuftrag.ParamByName('Durchmesser').AsString := EditDurchmesser.Text;
  FDQueryAddAuftrag.ParamByName('Zaehne').AsString := EditZaehne.Text;
  FDQueryAddAuftrag.ParamByName('Stueck').AsString := EditStueck.Text;
  FDQueryAddAuftrag.ParamByName('Auftragsart').AsInteger := GetAuftragsartId (ComboBoxAuftragsart.Text);
  FDQueryAddAuftrag.ExecSQL;

  for i := 1 to 15 do begin
    if (FAbteilungList[i].Text <> '')
        or (FMaschineList[i].Text <> '')
        or (FKommentarList[i].Text <> '')
        or (FVorgabeList[i].Text <> '')
        or (FWocheList[i].Text <> '')
        or FErledigtList[i].Checked then begin
      if Logging then
        TMSLogger.Debug('AddAuftrag Detail' + IntToStr(i) +
          ' AuftragId=' + IntToStr(GetAuftragId) + ' ' +
          ' Nummer=' + IntToStr(i) +
          ' Abteilung=' + FAbteilungList[i].Text + ' (' + IntToStr(GetAbteilungId (FAbteilungList[i].Text)) + ')' +
          ' Maschine=' + FMaschineList[i].Text + ' (' + IntToStr(GetMaschineId (FMaschineList[i].Text)) + ')' +
          ' Kommentar=' + FKommentarList[i].Text +
          ' Vorgabe=' + FVorgabeList[i].Text +
          ' Woche=' + FWocheList[i].Text +
          ' Erledigt=' + FErledigtList[i].Checked.ToString +
          ' Datum=' + FDatumList[i].Text);
      FDQueryAddPlanung.ParamByName('Auftrag').AsInteger := GetAuftragId;
      FDQueryAddPlanung.ParamByName('Nummer').AsInteger := i;
      FDQueryAddPlanung.ParamByName('Abteilung').AsInteger := GetAbteilungId (FAbteilungList[i].Text);
      FDQueryAddPlanung.ParamByName('Maschine').AsInteger := GetMaschineId (FMaschineList[i].Text);
      FDQueryAddPlanung.ParamByName('Kommentar').AsString := FKommentarList[i].Text;
      FDQueryAddPlanung.ParamByName('Vorgabe').AsFloat := StrToFloatDef (FVorgabeList[i].Text, 0);
      FDQueryAddPlanung.ParamByName('Woche').AsInteger := StrToIntDef (FWocheList[i].Text, 0);
      FDQueryAddPlanung.ParamByName('Erledigt').AsBoolean := FErledigtList[i].Checked;
      try
        TempDate := StrToDate(FDatumList[i].Text);
      except
        TempDate := 0;
      end;
      FDQueryAddPlanung.ParamByName('Datum').AsDate := TempDate;
      FDQueryAddPlanung.ExecSQL;
    end;
  end;
  Sleep(50);
  if Logging then
    TMSLogger.Debug('AddAuftrag end');
end;

procedure TFormAuftrag.AdvStringGridPlanungGroupCalc(Sender: TObject; ACol,
  FromRow, ToRow: Integer; var Res: Double);
var
  Kapazitaet,
  i : Integer;
begin
  FDQueryGetKapazitaet.ParamByName('Maschine').AsInteger := FMaschineId;
  FDQueryGetKapazitaet.ParamByName('Woche').AsInteger := StrToIntDef (AdvStringGridPlanung.Cells[1, FromRow-1], 0);
  {
  if AdvStringGridPlanung.Cells[1, FromRow-1] = '' then
    FDQueryGetKapazitaet.ParamByName('Woche').AsInteger := 0
  else
    FDQueryGetKapazitaet.ParamByName('Woche').AsString := AdvStringGridPlanung.Cells[1, FromRow-1];
    }
  FDQueryGetKapazitaet.Open;
  FDQueryGetKapazitaet.First;
  Kapazitaet := FDQueryGetKapazitaet.FieldByName('Kapazitaet').AsInteger;
  FDQueryGetKapazitaet.Close;

  Res := 0.0;
  try
    for i := FromRow to ToRow do
      Res := Res + AdvStringGridPlanung.Floats[ACol, i];
    AdvStringGridPlanung.Cells[1, FromRow-1] := 'Woche ' + AdvStringGridPlanung.Cells[1, FromRow-1] + ': '
        + FloatToStr(Res) + ' / ' + IntToStr(Kapazitaet);
    if Res > Kapazitaet then
      AdvStringGridPlanung.Colors[1, FromRow-1] := FormKonfiguration.ColorUeberbuchung;
  except
    exit;
  end;
end;

procedure TFormAuftrag.UpdateAuftrag;
var
  i: Integer;
  TempDate : TDateTime;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('UpdateAuftrag');
    TMSLogger.Debug('UpdateAuftrag' +
      ' ID=' + AuftragId.ToString +
      ' Kunde=' + ComboBoxKunde.Text + ' (' + IntToStr(GetKundeId (ComboBoxKunde.Text)) + ')' +
      ' AB-Nr=' + EditAbNr.Text +
      ' Artikel-Nr=' + EditArtikelNr.Text +
      ' Durchmesser=' + EditDurchmesser.Text +
      ' Z�hne=' + EditZaehne.Text +
      ' St�ck=' + EditStueck.Text +
      ' Auftragsart=' + ComboBoxAuftragsart.Text + ' (' + IntToStr(GetAuftragsartId (ComboBoxAuftragsart.Text)) + ')');
  end;

  FDQueryUpdateAuftrag.ParamByName('ID').AsInteger := AuftragId;
  FDQueryUpdateAuftrag.ParamByName('Kunde').AsInteger := GetKundeId (ComboBoxKunde.Text);
  FDQueryUpdateAuftrag.ParamByName('AB_Nr').AsString := EditAbNr.Text;
  FDQueryUpdateAuftrag.ParamByName('Artikel_Nr').AsString := EditArtikelNr.Text;
  FDQueryUpdateAuftrag.ParamByName('Durchmesser').AsString := EditDurchmesser.Text;
  FDQueryUpdateAuftrag.ParamByName('Zaehne').AsString := EditZaehne.Text;
  FDQueryUpdateAuftrag.ParamByName('Stueck').AsString := EditStueck.Text;
  FDQueryUpdateAuftrag.ParamByName('Auftragsart').AsInteger := GetAuftragsartId (ComboBoxAuftragsart.Text);
  FDQueryUpdateAuftrag.ExecSQL;

  for i := 1 to 15 do begin
    if  (FAbteilungList[i].Text = 'Alle')
        and (FMaschineList[i].Text = '')
        and (FKommentarList[i].Text = '')
        and (FVorgabeList[i].Text = '')
        and (FWocheList[i].Text = '')
        and not FErledigtList[i].Checked then begin
       // Feld gel�scht
      if Logging then
        TMSLogger.Debug('UpdateAuftrag Detail Feld gel�scht' +
          IntToStr(i) +
          ' ID=' + FIDList[i].Text);
      FDQueryDeletePlanung.ParamByName('ID').AsString := FIDList[i].Text;
      FDQueryDeletePlanung.ExecSQL;
    end
    else
    if (FAbteilungList[i].Text <> '')
        or (FMaschineList[i].Text <> '')
        or (FKommentarList[i].Text <> '')
        or (FVorgabeList[i].Text <> '')
        or (FWocheList[i].Text <> '')
        or FErledigtList[i].Checked then begin
      if FIdList[i].Text <> '' then begin
        if Logging then
          TMSLogger.Debug('UpdateAuftrag Detail ge�ndert ' + IntToStr (i) +
            ' ID=' + FIDList[i].Text +
            ' Auftrag=' + AuftragId.ToString +
            ' Nummer=' + i.ToString +
            ' Abteilung=' + FAbteilungList[i].Text + ' (' + IntToStr(GetAbteilungId (FAbteilungList[i].Text)) + ')' +
            ' Maschine=' + FMaschineList[i].Text + ' (' + IntToStr(GetMaschineId (FMaschineList[i].Text)) + ')' +
            ' Kommentar=' + FKommentarList[i].Text +
            ' Vorgabe=' + FVorgabeList[i].Text +
            ' Woche=' + FWocheList[i].Text +
            ' Erledigt=' + FErledigtList[i].Checked.ToString +
            ' Datum=' + FDatumList[i].Text);
        FDQueryUpdatePlanung.ParamByName('ID').AsString := FIDList[i].Text;
        FDQueryUpdatePlanung.ParamByName('Auftrag').AsInteger := AuftragId;
        FDQueryUpdatePlanung.ParamByName('Nummer').AsInteger := i;
        FDQueryUpdatePlanung.ParamByName('Abteilung').AsInteger := GetAbteilungId (FAbteilungList[i].Text);
        FDQueryUpdatePlanung.ParamByName('Maschine').AsInteger := GetMaschineId (FMaschineList[i].Text);
        FDQueryUpdatePlanung.ParamByName('Kommentar').AsString := FKommentarList[i].Text;
        FDQueryUpdatePlanung.ParamByName('Vorgabe').AsFloat := StrToFloatDef (FVorgabeList[i].Text, 0);
        FDQueryUpdatePlanung.ParamByName('Woche').AsInteger := StrToIntDef (FWocheList[i].Text, 0);
        FDQueryUpdatePlanung.ParamByName('Erledigt').AsBoolean := FErledigtList[i].Checked;
        try
          TempDate := StrToDate(FDatumList[i].Text);
        except
          TempDate := 0;
        end;
        FDQueryUpdatePlanung.ParamByName('Datum').AsDate := TempDate;
        FDQueryUpdatePlanung.ExecSQL;
      end
      else begin
        // Neuer Eintrag
        if Logging then
          TMSLogger.Debug('UpdateAuftrag Detail neu ' + IntToStr (i) +
            ' Auftrag=' + AuftragId.ToString +
            ' Nummer=' + i.ToString +
            ' Abteilung=' + FAbteilungList[i].Text + ' (' + IntToStr(GetAbteilungId (FAbteilungList[i].Text)) + ')' +
            ' Maschine=' + FMaschineList[i].Text + ' (' + IntToStr(GetMaschineId (FMaschineList[i].Text)) + ')' +
            ' Kommentar=' + FKommentarList[i].Text +
            ' Vorgabe=' + FVorgabeList[i].Text +
            ' Woche=' + FWocheList[i].Text +
            ' Erledigt=' + FErledigtList[i].Checked.ToString +
            ' Datum=' + FDatumList[i].Text);
        FDQueryAddPlanung.ParamByName('Auftrag').AsInteger := AuftragId;
        FDQueryAddPlanung.ParamByName('Nummer').AsInteger := i;
        FDQueryAddPlanung.ParamByName('Abteilung').AsInteger := GetAbteilungId (FAbteilungList[i].Text);
        FDQueryAddPlanung.ParamByName('Maschine').AsInteger := GetMaschineId (FMaschineList[i].Text);
        FDQueryAddPlanung.ParamByName('Kommentar').AsString := FKommentarList[i].Text;
        FDQueryAddPlanung.ParamByName('Vorgabe').AsFloat := StrToFloatDef (FVorgabeList[i].Text, 0);
        FDQueryAddPlanung.ParamByName('Woche').AsInteger := StrToIntDef (FWocheList[i].Text, 0);
        FDQueryAddPlanung.ParamByName('Erledigt').AsBoolean := FErledigtList[i].Checked;
        try
          TempDate := StrToDate(FDatumList[i].Text);
        except
          TempDate := 0;
        end;
        FDQueryAddPlanung.ParamByName('Datum').AsDate := TempDate;
        FDQueryAddPlanung.ExecSQL;
      end;
    end
    else if (FIdList[i].Text <> '') then begin
      // Feld gel�scht
      if Logging then
        TMSLogger.Debug('UpdateAuftrag Detail Feld gel�scht' +
          IntToStr(i) +
          ' ID=' + FIDList[i].Text);
      FDQueryDeletePlanung.ParamByName('ID').AsString := FIdList[i].Text;
      FDQueryDeletePlanung.ExecSQL;
    end;
  end;
  Sleep(50);
  if Logging then
    TMSLogger.Debug('UpdateAuftrag end');
end;

procedure TFormAuftrag.LoeschePlanung(Zeile: Integer);
var
  i : Integer;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('LoeschePlanung');
    TMSLogger.Debug('LoeschePlanung Zeile=' + Zeile.ToString);
  end;
  if FIdList[Zeile].Text <> '' then begin
    FDQueryDeletePlanung.ParamByName('ID').AsString := FIdList[Zeile].Text;
    FDQueryDeletePlanung.ExecSQL;
  end;
  Sleep(50);
  for i := Zeile to 14 do begin
    FIdList[i].Text := FIdList[i+1].Text;
    FAbteilungList[i].ItemIndex := FAbteilungList[i+1].ItemIndex;
    FMaschineList[i].ItemIndex := FMaschineList[i+1].ItemIndex;
    FKommentarList[i].Text := FKommentarList[i+1].Text;
    FVorgabeList[i].Text := FVorgabeList[i+1].Text;
    FWocheList[i].Text := FWocheList[i+1].Text;
    FErledigtList[i].Checked := FErledigtList[i+1].Checked;
    FDatumList[i].Text := FDatumList[i+1].Text;
  end;
  FIdList[15].Text := '';
  FAbteilungList[15].ItemIndex := -1;
  FMaschineList[15].ItemIndex := -1;
  FKommentarList[15].Text := '';
  FVorgabeList[15].Text := '';
  FWocheList[15].Text := '';
  FErledigtList[15].Checked := False;
  FDatumList[15].Text := '';
  if Logging then
    TMSLogger.Debug('LoeschePlanung end');
end;

procedure TFormAuftrag.UpdateListe(Zeile : Integer);
var
  Maschine : String;
  row : Integer;
begin
  if not FUpdateEnabled then
    exit;

  AdvStringGridPlanung.UnGroup;
  for row := 1 to AdvStringGridPlanung.RowCount - 1 do begin
    AdvStringGridPlanung.ClearRows(1, 1);
    AdvStringGridPlanung.RemoveRows(1, 1);
  end;
  AdvStringGridPlanung.Clear;

  AdvStringGridPlanung.Cells[1, 0] := 'Woche';
  AdvStringGridPlanung.Cells[2, 0] := 'ID';
  AdvStringGridPlanung.Cells[3, 0] := 'Kunde';
  AdvStringGridPlanung.Cells[4, 0] := 'AB-Nr';
  AdvStringGridPlanung.Cells[5, 0] := 'Artikel-Nr';
  AdvStringGridPlanung.Cells[6, 0] := 'Durchmesser';
  AdvStringGridPlanung.Cells[7, 0] := 'Z�hne';
  AdvStringGridPlanung.Cells[8, 0] := 'St�ck';
  AdvStringGridPlanung.Cells[9, 0] := 'Auftragsart';
  AdvStringGridPlanung.Cells[10, 0] := 'Abteilung';
  AdvStringGridPlanung.Cells[11, 0] := 'Maschine';
  AdvStringGridPlanung.Cells[12, 0] := 'Kommentar';
  AdvStringGridPlanung.Cells[13, 0] := 'Vorgabe';
  AdvStringGridPlanung.ColCount := 14;

  AdvStringGridPlanung.Grouping.MergeHeader := True;
  // AdvStringGridPlanung.Grouping.MergeSummary := True;
  // AdvStringGridPlanung.Grouping.Summary := True;
  // AdvStringGridPlanung.Grouping.SummaryLine := True;

  Maschine := FMaschineList[Zeile].Text;
  if Maschine = '' then
    exit;
  FMaschineId := GetMaschineId(Maschine);
  FDQueryGetOffeneAuftraege.ParamByName('Maschine').AsInteger := FMaschineId;
  FDQueryGetOffeneAuftraege.ParamByName('Woche').AsInteger := WeekOfTheYear(Now) + FormKonfiguration.Vorschau;
  FDQueryGetOffeneAuftraege.Open;
  FDQueryGetOffeneAuftraege.First;
  row := 1;

  try
    AdvStringGridPlanung.BeginUpdate;
    while not FDQueryGetOffeneAuftraege.Eof do begin
      AdvStringGridPlanung.AddRow;
      if FDQueryGetOffeneAuftraege.FieldByName('Woche').AsInteger > 0 then
        AdvStringGridPlanung.Cells[1, row] := FDQueryGetOffeneAuftraege.FieldByName('Woche').AsString;
      AdvStringGridPlanung.Cells[2, row] := FDQueryGetOffeneAuftraege.FieldByName('ID').AsString;
      AdvStringGridPlanung.Cells[3, row] := FDQueryGetOffeneAuftraege.FieldByName('Kunde').AsString;
      AdvStringGridPlanung.Cells[4, row] := FDQueryGetOffeneAuftraege.FieldByName('AB_Nr').AsString;
      AdvStringGridPlanung.Cells[5, row] := FDQueryGetOffeneAuftraege.FieldByName('Artikel_Nr').AsString;
      AdvStringGridPlanung.Cells[6, row] := FDQueryGetOffeneAuftraege.FieldByName('Durchmesser').AsString;
      AdvStringGridPlanung.Cells[7, row] := FDQueryGetOffeneAuftraege.FieldByName('Zaehne').AsString;
      AdvStringGridPlanung.Cells[8, row] := FDQueryGetOffeneAuftraege.FieldByName('Stueck').AsString;
      AdvStringGridPlanung.Cells[9, row] := FDQueryGetOffeneAuftraege.FieldByName('Auftragsart').AsString;
      AdvStringGridPlanung.Cells[10, row] := FDQueryGetOffeneAuftraege.FieldByName('Abteilung').AsString;
      AdvStringGridPlanung.Cells[11, row] := FDQueryGetOffeneAuftraege.FieldByName('Maschine').AsString;
      AdvStringGridPlanung.Cells[12, row] := FDQueryGetOffeneAuftraege.FieldByName('Kommentar').AsString;
      if FDQueryGetOffeneAuftraege.FieldByName('Vorgabe').AsFloat > 0 then
        AdvStringGridPlanung.Cells[13, row] := FDQueryGetOffeneAuftraege.FieldByName('Vorgabe').AsString;
      row := row + 1;
      FDQueryGetOffeneAuftraege.Next;
    end;
  finally
    AdvStringGridPlanung.EndUpdate;
  end;
  FDQueryGetOffeneAuftraege.Close;

  AdvStringGridPlanung.Group(1);
  // AdvStringGridPlanung.GroupSum(12);
  AdvStringGridPlanung.GroupCustomCalc(12);
  AdvStringGridPlanung.AutoSizeColumns(true, 4);
end;

procedure TFormAuftrag.AbteilungChanged(Sender: TObject);
var
  Source,
  Temp : String;
  Zeile : Integer;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('AbteilungChanged');
  end;

  // 123456789012345678
  // ComboBoxAbteilung1
  Source := (Sender as TComboBox).Name;
  Temp := Copy(Source, 18, Length(Source) - 17);
  Zeile := StrToInt(Temp);

  if Logging then
    TMSLogger.Debug('AbteilungChanged Zeile=' + IntToStr(Zeile) + ' -> ' + FAbteilungList[Zeile].Text);

  // Update Maschine
  FMaschinenListe.Clear;

  if FAbteilungList[Zeile].ItemIndex > 1 then begin
    FDQueryMaschineInAbteilung.ParamByName('Abteilung').AsInteger := GetAbteilungId (FAbteilungList[Zeile].Text);
    FDQueryMaschineInAbteilung.Active := True;
    FDQueryMaschineInAbteilung.First;
    while not FDQueryMaschineInAbteilung.Eof do begin
      FMaschinenListe.Add(FDQueryMaschineInAbteilung.FieldByName('Maschine').AsString);
      FDQueryMaschineInAbteilung.Next;
    end;
    FDQueryMaschineInAbteilung.Close;
  end
  else begin
    FDQueryMaschine.Active := True;
    FDQueryMaschine.First;
    while not FDQueryMaschine.Eof do begin
      FMaschinenListe.Add(FDQueryMaschine.FieldByName('Maschine').AsString);
      FDQueryMaschine.Next;
    end;
    FDQueryMaschine.Close;
  end;

  FMaschineList[Zeile].Items := FMaschinenListe;
  FMaschineList[Zeile].ItemIndex := -1;

  UpdateListe(Zeile);

  if Logging then
    TMSLogger.Debug('AbteilungChanged end');
end;

procedure TFormAuftrag.MaschineChanged(Sender: TObject);
var
  Source,
  Temp : String;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('MaschineChanged');
  end;

  // 12345678901234567
  // ComboBoxMaschine1
  Source := (Sender as TComboBox).Name;
  Temp := Copy(Source, 17, Length(Source) - 16);

  if Logging then
    TMSLogger.Debug('MaschineChanged Zeile=' + Temp);

  UpdateListe(StrToInt(Temp));

  if Logging then
    TMSLogger.Debug('MaschineChanged end');
end;

procedure TFormAuftrag.VorgabeChanged(Sender: TObject);
var
  Source,
  Temp : String;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('VorgabeChanged');
  end;

  // 123456789012
  // EditVorgabe1
  Source := (Sender as TEdit).Name;
  Temp := Copy(Source, 12, Length(Source) - 11);

  if Logging then
    TMSLogger.Debug('VorgabeChanged Zeile=' + Temp);

  UpdateListe(StrToInt(Temp));

  if Logging then
    TMSLogger.Debug('VorgabeChanged end');
end;

procedure TFormAuftrag.VorgabeClick(Sender: TObject);
var
  Source,
  Temp : String;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('VorgabeClick');
  end;

  // 123456789012
  // EditVorgabe1
  Source := (Sender as TEdit).Name;
  Temp := Copy(Source, 12, Length(Source) - 11);

  if Logging then
    TMSLogger.Debug('VorgabeClick Zeile=' + Temp);

  UpdateListe(StrToInt(Temp));

  if Logging then
    TMSLogger.Debug('VorgabeClick end');
end;

procedure TFormAuftrag.WocheChanged(Sender: TObject);
var
  Source,
  Temp : String;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('WocheChanged');
  end;

  // 1234567890
  // EditWoche1
  Source := (Sender as TEdit).Name;
  Temp := Copy(Source, 10, Length(Source) - 9);

  if Logging then
    TMSLogger.Debug('WocheChanged Zeile=' + Temp);

  UpdateListe(StrToInt(Temp));

  if Logging then
    TMSLogger.Debug('WocheChanged end');
end;

procedure TFormAuftrag.WocheClick(Sender: TObject);
var
  Source,
  Temp : String;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('WocheClick');
  end;

  // 1234567890
  // EditWoche1
  Source := (Sender as TEdit).Name;
  Temp := Copy(Source, 10, Length(Source) - 9);

  if Logging then
    TMSLogger.Debug('WocheClick Zeile=' + Temp);

  UpdateListe(StrToInt(Temp));

  if Logging then
    TMSLogger.Debug('WocheClick end');
end;

procedure TFormAuftrag.KommentarClick(Sender: TObject);
var
  Source,
  Temp : String;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('KommentarClick');
  end;

  // 12345678901234
  // EditKommentar1
  Source := (Sender as TEdit).Name;
  Temp := Copy(Source, 14, Length(Source) - 13);

  if Logging then
    TMSLogger.Debug('KommentarClick Zeile=' + Temp);

  UpdateListe(StrToInt(Temp));

  if Logging then
    TMSLogger.Debug('KommentarClick end');
end;

procedure TFormAuftrag.SpeedButtonDeleteClick(Sender: TObject);
var
  Source,
  Temp : String;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('SpeedButtonDeleteClick');
  end;

  // 123456789012345678
  // SpeedButtonDelete1
  Source := (Sender as TSpeedButton).Name;
  Temp := Copy(Source, 18, Length(Source) - 17);

  if Logging then
    TMSLogger.Debug('SpeedButtonDeleteClick Zeile=' + Temp);

  LoeschePlanung (StrToIntDef(Temp, 0));

  if Logging then
    TMSLogger.Debug('SpeedButtonDeleteClick end');
end;

procedure TFormAuftrag.CheckBoxErledigtClick(Sender: TObject);
var
  Source,
  Temp : String;
  Zeile : Integer;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('CheckBoxErledigt');
  end;

  // 12345678901234567
  // CheckBoxErledigt1
  Source := (Sender as TCheckBox).Name;
  Temp := Copy(Source, 17, Length(Source) - 16);
  Zeile := StrToIntDef(Temp, 0);

  if Logging then
    TMSLogger.Debug('CheckBoxErledigtClick Zeile=' + Temp);

  if FErledigtList[Zeile].Checked then
    FDatumList[Zeile].Text := DateToStr(Now)
  else
    FDatumList[Zeile].Text := '';

  if Logging then
    TMSLogger.Debug('CheckBoxErledigt end');
end;

procedure TFormAuftrag.ButtonCancelClick(Sender: TObject);
begin
  if Logging then
    TMSLogger.Debug('ButtonCancelClick');
  Close;
end;

procedure TFormAuftrag.ButtonDeleteClick(Sender: TObject);
var
  Zeile : Integer;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('ButtonDeleteClick ID=' + AuftragId.ToString);
  end;

  if MessageDlg('Wollen Sie den Auftrag wirklich l�schen ?', mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then begin
    for Zeile := 1 to 15 do
      LoeschePlanung(Zeile);
    FDQueryDeleteAuftrag.ParamByName('Id').AsInteger := AuftragId;
    FDQueryDeleteAuftrag.ExecSQL;
  end;
  Sleep(50);
  Close;

  if Logging then
    TMSLogger.Debug('ButtonDelete end');
end;

procedure TFormAuftrag.ButtonOkClick(Sender: TObject);
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('ButtonOkClick');
  end;

  // Datensatz speichern
  if AuftragId = -1 then begin
    // Neuer Auftrag
    AddAuftrag;
  end
  else begin
    // Auftrag �ndern
    UpdateAuftrag;
  end;
  sleep(50);
  Close;
end;

procedure TFormAuftrag.ComboBoxAuftragsartChange(Sender: TObject);
begin
  FormAuftrag.Color := clWhite;

  if Logging then
    TMSLogger.Debug('ComboBoxAuftragsartChange -> ' + ComboBoxAuftragsart.Text);

  if ComboBoxAuftragsart.Text = 'Neues Werkzeug' then
    FormAuftrag.Color := FormKonfiguration.ColorNeu;
  if ComboBoxAuftragsart.Text = 'Express' then
    FormAuftrag.Color := FormKonfiguration.ColorExpress;
  if ComboBoxAuftragsart.Text = 'Reklamation' then
    FormAuftrag.Color := FormKonfiguration.ColorReklamation;
end;

procedure TFormAuftrag.ComboBoxKundeExit(Sender: TObject);
var
  Kunde : String;
  Index : Integer;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('ComboBoxKundeExit Kunde=' + ComboBoxKunde.Text);
  end;
  //
  Kunde := ComboBoxKunde.Text;
  Index := ComboBoxKunde.ItemIndex;
  if Index = -1 then begin
    // Neuer Kunde -> Speichern
    FDQueryAddKunde.ParamByName('Kunde').AsString := Kunde;
    FDQueryAddKunde.ExecSQL;
  end;
  if Logging then
    TMSLogger.Debug('ComboBoxKundeExit end');
end;

procedure TFormAuftrag.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('FormClose');
  end;
  FDConnection.Close;
end;

procedure TFormAuftrag.FormCreate(Sender: TObject);
begin
  if logging then
    TMSLogger.Debug('FormCreate');

  FormAuftrag.Position := poScreenCenter;
  FUpdateEnabled := False;
  FDConnection.Params.Add('DriverID=MSAcc');
  FDConnection.Params.Add('Database=' + FormMain.DbName);
  // FDConnection.Params.Add('Database=C:\Users\Andre\Documents\Embarcadero\Studio\Projekte\Gloor AG\Gloor.accdb');
  FKundenListe := TStringList.Create;
  FAbteilungsListe := TStringList.Create;
  FMaschinenListe := TStringList.Create;
  FAuftragsart := TStringList.Create;

  FButtonDelete[1] := SpeedButtonDelete1;
  FButtonDelete[2] := SpeedButtonDelete2;
  FButtonDelete[3] := SpeedButtonDelete3;
  FButtonDelete[4] := SpeedButtonDelete4;
  FButtonDelete[5] := SpeedButtonDelete5;
  FButtonDelete[6] := SpeedButtonDelete6;
  FButtonDelete[7] := SpeedButtonDelete7;
  FButtonDelete[8] := SpeedButtonDelete8;
  FButtonDelete[9] := SpeedButtonDelete9;
  FButtonDelete[10] := SpeedButtonDelete10;
  FButtonDelete[11] := SpeedButtonDelete11;
  FButtonDelete[12] := SpeedButtonDelete12;
  FButtonDelete[13] := SpeedButtonDelete13;
  FButtonDelete[14] := SpeedButtonDelete14;
  FButtonDelete[15] := SpeedButtonDelete15;
  FIdList[1] := Edit1;
  FIdList[2] := Edit2;
  FIdList[3] := Edit3;
  FIdList[4] := Edit4;
  FIdList[5] := Edit5;
  FIdList[6] := Edit6;
  FIdList[7] := Edit7;
  FIdList[8] := Edit8;
  FIdList[9] := Edit9;
  FIdList[10] := Edit10;
  FIdList[11] := Edit11;
  FIdList[12] := Edit12;
  FIdList[13] := Edit13;
  FIdList[14] := Edit14;
  FIdList[15] := Edit15;
  FAbteilungList[1] := ComboBoxAbteilung1;
  FAbteilungList[2] := ComboBoxAbteilung2;
  FAbteilungList[3] := ComboBoxAbteilung3;
  FAbteilungList[4] := ComboBoxAbteilung4;
  FAbteilungList[5] := ComboBoxAbteilung5;
  FAbteilungList[6] := ComboBoxAbteilung6;
  FAbteilungList[7] := ComboBoxAbteilung7;
  FAbteilungList[8] := ComboBoxAbteilung8;
  FAbteilungList[9] := ComboBoxAbteilung9;
  FAbteilungList[10] := ComboBoxAbteilung10;
  FAbteilungList[11] := ComboBoxAbteilung11;
  FAbteilungList[12] := ComboBoxAbteilung12;
  FAbteilungList[13] := ComboBoxAbteilung13;
  FAbteilungList[14] := ComboBoxAbteilung14;
  FAbteilungList[15] := ComboBoxAbteilung15;
  FMaschineList[1] := ComboBoxMaschine1;
  FMaschineList[2] := ComboBoxMaschine2;
  FMaschineList[3] := ComboBoxMaschine3;
  FMaschineList[4] := ComboBoxMaschine4;
  FMaschineList[5] := ComboBoxMaschine5;
  FMaschineList[6] := ComboBoxMaschine6;
  FMaschineList[7] := ComboBoxMaschine7;
  FMaschineList[8] := ComboBoxMaschine8;
  FMaschineList[9] := ComboBoxMaschine9;
  FMaschineList[10] := ComboBoxMaschine10;
  FMaschineList[11] := ComboBoxMaschine11;
  FMaschineList[12] := ComboBoxMaschine12;
  FMaschineList[13] := ComboBoxMaschine13;
  FMaschineList[14] := ComboBoxMaschine14;
  FMaschineList[15] := ComboBoxMaschine15;
  FKommentarList[1] := EditKommentar1;
  FKommentarList[2] := EditKommentar2;
  FKommentarList[3] := EditKommentar3;
  FKommentarList[4] := EditKommentar4;
  FKommentarList[5] := EditKommentar5;
  FKommentarList[6] := EditKommentar6;
  FKommentarList[7] := EditKommentar7;
  FKommentarList[8] := EditKommentar8;
  FKommentarList[9] := EditKommentar9;
  FKommentarList[10] := EditKommentar10;
  FKommentarList[11] := EditKommentar11;
  FKommentarList[12] := EditKommentar12;
  FKommentarList[13] := EditKommentar13;
  FKommentarList[14] := EditKommentar14;
  FKommentarList[15] := EditKommentar15;
  FVorgabeList[1] := EditVorgabe1;
  FVorgabeList[2] := EditVorgabe2;
  FVorgabeList[3] := EditVorgabe3;
  FVorgabeList[4] := EditVorgabe4;
  FVorgabeList[5] := EditVorgabe5;
  FVorgabeList[6] := EditVorgabe6;
  FVorgabeList[7] := EditVorgabe7;
  FVorgabeList[8] := EditVorgabe8;
  FVorgabeList[9] := EditVorgabe9;
  FVorgabeList[10] := EditVorgabe10;
  FVorgabeList[11] := EditVorgabe11;
  FVorgabeList[12] := EditVorgabe12;
  FVorgabeList[13] := EditVorgabe13;
  FVorgabeList[14] := EditVorgabe14;
  FVorgabeList[15] := EditVorgabe15;
  FWocheList[1] := EditWoche1;
  FWocheList[2] := EditWoche2;
  FWocheList[3] := EditWoche3;
  FWocheList[4] := EditWoche4;
  FWocheList[5] := EditWoche5;
  FWocheList[6] := EditWoche6;
  FWocheList[7] := EditWoche7;
  FWocheList[8] := EditWoche8;
  FWocheList[9] := EditWoche9;
  FWocheList[10] := EditWoche10;
  FWocheList[11] := EditWoche11;
  FWocheList[12] := EditWoche12;
  FWocheList[13] := EditWoche13;
  FWocheList[14] := EditWoche14;
  FWocheList[15] := EditWoche15;
  FErledigtList[1] := CheckBoxErledigt1;
  FErledigtList[2] := CheckBoxErledigt2;
  FErledigtList[3] := CheckBoxErledigt3;
  FErledigtList[4] := CheckBoxErledigt4;
  FErledigtList[5] := CheckBoxErledigt5;
  FErledigtList[6] := CheckBoxErledigt6;
  FErledigtList[7] := CheckBoxErledigt7;
  FErledigtList[8] := CheckBoxErledigt8;
  FErledigtList[9] := CheckBoxErledigt9;
  FErledigtList[10] := CheckBoxErledigt10;
  FErledigtList[11] := CheckBoxErledigt11;
  FErledigtList[12] := CheckBoxErledigt12;
  FErledigtList[13] := CheckBoxErledigt13;
  FErledigtList[14] := CheckBoxErledigt14;
  FErledigtList[15] := CheckBoxErledigt15;
  FDatumList[1] := EditDatum1;
  FDatumList[2] := EditDatum2;
  FDatumList[3] := EditDatum3;
  FDatumList[4] := EditDatum4;
  FDatumList[5] := EditDatum5;
  FDatumList[6] := EditDatum6;
  FDatumList[7] := EditDatum7;
  FDatumList[8] := EditDatum8;
  FDatumList[9] := EditDatum9;
  FDatumList[10] := EditDatum10;
  FDatumList[11] := EditDatum11;
  FDatumList[12] := EditDatum12;
  FDatumList[13] := EditDatum13;
  FDatumList[14] := EditDatum14;
  FDatumList[15] := EditDatum15;
end;

procedure TFormAuftrag.FormShow(Sender: TObject);
var
  Count: integer;
  i: Integer;
begin
  if Logging then begin
    TMSLogger.LogSeparator;
    TMSLogger.Debug('FormShow');
  end;

  FUpdateEnabled := False;
  FDConnection.Open;
  FKundenListe.Clear;
  FAbteilungsListe.Clear;
  FMaschinenListe.Clear;
  FAuftragsart.Clear;

  for i := 1 to 15 do
    if FormKonfiguration.LoeschenErlaubt then
      FButtonDelete[i].Visible := True
    else
      FButtonDelete[i].Visible := False;

  AdvStringGridPlanung.Clear;
  ComboBoxKunde.Text := '';

  FDQueryKunde.Active := True;
  FDQueryKunde.First;
  while not FDQueryKunde.Eof do begin
    FKundenListe.Add(FDQueryKunde.FieldByName('Kunde').AsString);
    FDQueryKunde.Next;
  end;
  FDQueryKunde.Active := False;
  ComboBoxKunde.Items := FKundenListe;
  ComboBoxKunde.ItemIndex := -1;

  FDQueryAbteilung.Active := True;
  FDQueryAbteilung.First;
  while not FDQueryAbteilung.Eof do begin
    FAbteilungsListe.Add(FDQueryAbteilung.FieldByName('Abteilung').AsString);
    FDQueryAbteilung.Next;
  end;
  FDQueryAbteilung.Active := False;

  FDQueryMaschine.Active := True;
  FDQueryMaschine.First;
  while not FDQueryMaschine.Eof do begin
    FMaschinenListe.Add(FDQueryMaschine.FieldByName('Maschine').AsString);
    FDQueryMaschine.Next;
  end;
  FDQueryMaschine.Close;

  ComboBoxKunde.ItemIndex := -1;
  EditAbNr.Text := '';
  EditArtikelNr.Text := '';
  EditDurchmesser.Text := '';
  EditZaehne.Text := '';
  EditStueck.Text := '';
  ComboBoxAuftragsart.ItemIndex := -1;

  for i := 1 to 15 do begin
    FIdList[i].Text := '';
    FAbteilungList[i].Items := FAbteilungsListe;
    FAbteilungList[i].ItemIndex := -1;
    FMaschineList[i].Items := FMaschinenListe;
    FMaschineList[i].ItemIndex := -1;
    FKommentarList[i].Text := '';
    FVorgabeList[i].Text := '';
    FWocheList[i].Text := '';
    FErledigtList[i].Checked := False;
    FDatumList[i].Text := '';
  end;

  FDQueryAuftragsart.Active := True;
  FDQueryAuftragsart.First;
  while not FDQueryAuftragsart.Eof do begin
    FAuftragsart.Add(FDQueryAuftragsart.FieldByName('Auftragsart').AsString);
    FDQueryAuftragsart.Next;
  end;
  FDQueryAuftragsart.Close;
  ComboBoxAuftragsart.Items := FAuftragsart;
  ComboBoxAuftragsart.ItemIndex := 0;

  ComboBoxKunde.SetFocus;

  FormAuftrag.Color := clWhite;

  if AuftragId >= 0 then begin
    if Logging then
      TMSLogger.Debug('FormShow AuftragID= ' + AuftragId.ToString);
    // Auftrag bearbeiten
    if FormKonfiguration.LoeschenErlaubt then
      ButtonDelete.Visible := True
    else
      ButtonDelete.Visible := False;

    FDQueryAuftrag.ParamByName('Id').AsInteger := AuftragId;
    FDQueryAuftrag.Open;
    FDQueryAuftrag.First;
    ComboBoxKunde.ItemIndex := ComboBoxKunde.Items.IndexOf(GetKunde(FDQueryAuftrag.FieldByName('Kunde').AsInteger));
    EditAbNr.Text := FDQueryAuftrag.FieldByName('AB_Nr').AsString;
    EditArtikelNr.Text := FDQueryAuftrag.FieldByName('Artikel_Nr').AsString;
    EditDurchmesser.Text := FDQueryAuftrag.FieldByName('Durchmesser').AsString;
    EditZaehne.Text := FDQueryAuftrag.FieldByName('Zaehne').AsString;
    EditStueck.Text := FDQueryAuftrag.FieldByName('Stueck').AsString;
    ComboBoxAuftragsart.ItemIndex := ComboBoxAuftragsart.Items.IndexOf(GetAuftragsart(FDQueryAuftrag.FieldByName('Auftragsart').AsInteger));

    FormAuftrag.Color := clWhite;
    if GetAuftragsart(FDQueryAuftrag.FieldByName('Auftragsart').AsInteger) = 'Neues Werkzeug' then
      FormAuftrag.Color := FormKonfiguration.ColorNeu;
    if GetAuftragsart(FDQueryAuftrag.FieldByName('Auftragsart').AsInteger)  = 'Express' then
      FormAuftrag.Color := FormKonfiguration.ColorExpress;
    if GetAuftragsart(FDQueryAuftrag.FieldByName('Auftragsart').AsInteger)  = 'Reklamation' then
      FormAuftrag.Color := FormKonfiguration.ColorReklamation;

    if Logging then
      TMSLogger.Debug('FormShow' +
        ' ID=' + AuftragId.ToString +
        ' AB-Nr=' + EditAbNr.Text +
        ' Artikel-Nr=' + EditArtikelNr.Text +
        ' Durchmesser=' + EditDurchmesser.Text +
        ' Z�hne=' + EditZaehne.Text +
        ' St�ck=' + EditStueck.Text +
        ' Auftragsart=' + GetAuftragsart(FDQueryAuftrag.FieldByName('Auftragsart').AsInteger));

    FDQueryAuftrag.Close;

    Count := 1;
    FDQueryPlanung.ParamByName('AuftragId').AsInteger := AuftragId;
    FDQueryPlanung.Open;
    FDQueryPlanung.First;
    while not FDQueryPlanung.Eof do begin
      FIdList[Count].Text := FDQueryPlanung.FieldByName('ID').AsString;
      FAbteilungList[Count].ItemIndex := FAbteilungList[Count].Items.IndexOf(GetAbteilung(FDQueryPlanung.FieldByName('Abteilung').AsInteger));
      FMaschineList[Count].ItemIndex := FMaschineList[Count].Items.IndexOf(GetMaschine(FDQueryPlanung.FieldByName('Maschine').AsInteger));
      FKommentarList[Count].Text := FDQueryPlanung.FieldByName('Kommentar').AsString;
      FVorgabeList[Count].Text := FDQueryPlanung.FieldByName('Vorgabe').AsString;
      FWocheList[Count].Text := FDQueryPlanung.FieldByName('Woche').AsString;
      FErledigtList[Count].Checked := FDQueryPlanung.FieldByName('Erledigt').AsBoolean;
      if FDQueryPlanung.FieldByName('Datum').AsDateTime = 0 then
        FDatumList[Count].Text := ''
      else
        FDatumList[Count].Text := FDQueryPlanung.FieldByName('Datum').AsString;

      if Logging then
        TMSLogger.Debug('FormShow Detail' +
          ' ID=' + FIdList[Count].Text +
          ' Abteilung=' + GetAbteilung(FDQueryPlanung.FieldByName('Abteilung').AsInteger) + ' (' + FDQueryPlanung.FieldByName('Abteilung').AsString + ')' +
          ' Maschine=' + GetMaschine(FDQueryPlanung.FieldByName('Maschine').AsInteger) + ' (' + FDQueryPlanung.FieldByName('Maschine').AsString + ')' +
          ' Kommentar=' + FKommentarList[Count].Text +
          ' Woche=' + FWocheList[Count].Text +
          ' Erledigt=' + FErledigtList[Count].Checked.ToString +
          ' Datum=' + FDatumList[Count].Text);

      Count := Count + 1;
      FDQueryPlanung.Next;
    end;
    FDQueryPlanung.Close;
  end
  else
    ButtonDelete.Visible := False;

  FUpdateEnabled := True;

  if Logging then
    TMSLogger.Debug('FormShow end');
end;

end.
